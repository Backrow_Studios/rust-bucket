﻿using UnityEngine;
using System.Collections;

public class Flash : MonoBehaviour {

	float alpha = 0f;
	public float decaySpeed = .02f;

	void Start () {
	
	}

	public void DoFlash(){
		alpha = 1;
		Color c = renderer.material.color;
		c.a = alpha;
		renderer.material.color = c;
	}

	void Update () {
		if (Input.GetKeyDown(KeyCode.F)){
			DoFlash();
		}
		if (alpha < .02f){
			if(renderer.enabled)
				renderer.enabled = false;
		}
		else{
			if(!renderer.enabled)
				renderer.enabled = true;
			alpha -= decaySpeed * Time.deltaTime * 60;
			Color c = renderer.material.color;
			c.a = alpha;
			renderer.material.color = c;
		}
	}
}
