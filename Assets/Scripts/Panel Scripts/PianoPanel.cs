﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PianoPanel : Panel {

	public GameObject SlideBody;

	public List<GameObject> graphs;
	List<LineGraph> graphList;
	public float graphTween;

	public float slideDistance;
	public float slideSpeed;

	public AudioClip N1;
	public AudioClip N2;
	public AudioClip N3;
	public AudioClip N4;
	public AudioClip N5;
	public AudioClip N6;
	public AudioClip N7;
	public AudioClip N8;
	public AudioClip N9;
	public AudioClip N10;
	public AudioClip N11;
	public AudioClip N12;
	public AudioClip N13;
	public AudioClip E1;
	public AudioClip E2;
	public AudioClip E3;
	public AudioClip E4;
	public AudioClip E5;
	public AudioClip E6;
	public AudioClip E7;
	public AudioClip E8;
	public AudioClip E9;
	public AudioClip E10;
	public AudioClip E11;
	public AudioClip E12;
	public AudioClip E13;



	public List<ButtonScript> buttonList = new List<ButtonScript>();
	public List<ButtonScript> toggleButtons = new List<ButtonScript>();

	public List<AudioClip> NList = new List<AudioClip>();
	public List<AudioClip> EList = new List<AudioClip>();

	void Start(){
		Normal ();
		base.Start();
		graphList = new List<LineGraph>();
		foreach (GameObject g in graphs){
			graphList.Add (g.GetComponent<LineGraph>());
		}

		Vector3 t = SlideBody.transform.localPosition;
		t.x -= slideDistance;
		SlideBody.transform.localPosition = t;

		FillButtonList ();
		FillSoundList();
		Normal ();
	}

	void FillButtonList(){
		buttonList.Add (GameObject.Find("Low_C").GetComponent<ButtonScript>());
		buttonList.Add (GameObject.Find("C#").GetComponent<ButtonScript>());
		buttonList.Add (GameObject.Find("D").GetComponent<ButtonScript>());
		buttonList.Add (GameObject.Find("Eb").GetComponent<ButtonScript>());
		buttonList.Add (GameObject.Find("E").GetComponent<ButtonScript>());
		buttonList.Add (GameObject.Find("F").GetComponent<ButtonScript>());
		buttonList.Add (GameObject.Find("F#").GetComponent<ButtonScript>());
		buttonList.Add (GameObject.Find("G").GetComponent<ButtonScript>());
		buttonList.Add (GameObject.Find("G#").GetComponent<ButtonScript>());
		buttonList.Add (GameObject.Find("A").GetComponent<ButtonScript>());
		buttonList.Add (GameObject.Find("Bb").GetComponent<ButtonScript>());
		buttonList.Add (GameObject.Find("B").GetComponent<ButtonScript>());
		buttonList.Add (GameObject.Find("High_C").GetComponent<ButtonScript>());

		toggleButtons.Add (GameObject.Find("NORMAL").GetComponent<ButtonScript>());
		toggleButtons.Add (GameObject.Find("ELECTRIC").GetComponent<ButtonScript>());

	}
		              
	void FillSoundList(){
		EList.Add (E1);
		EList.Add (E2);
		EList.Add (E3);
		EList.Add (E4);
		EList.Add (E5);
		EList.Add (E6);
		EList.Add (E7);
		EList.Add (E8);
		EList.Add (E9);
		EList.Add (E10);
		EList.Add (E11);
		EList.Add (E12);
		EList.Add (E13);

		NList.Add (N1);
		NList.Add (N2);
		NList.Add (N3);
		NList.Add (N4);
		NList.Add (N5);
		NList.Add (N6);
		NList.Add (N7);
		NList.Add (N8);
		NList.Add (N9);
		NList.Add (N10);
		NList.Add (N11);
		NList.Add (N12);
		NList.Add (N13);

	}

	int heldbutton = -1;

	public override void ClickOn(int v){
		if (v < graphList.Count){
			heldbutton = v;

			if (currentPower > 0){
				graphList[v].wavelength = Random.Range (.1f,20);
				graphList[v].amplitude = Random.Range (.4f,1f);
				graphList[v].scrollspeed = Random.Range (.05f,.5f);
			}
		}
		if (v == 10){
			Normal ();

			foreach (ButtonScript b in toggleButtons){
				if (b.sendVal != v){
					b.TurnOff();
				}
			}
		}
		else if (v == 11){
			Electric();

			foreach (ButtonScript b in toggleButtons){
				if (b.sendVal != v){
					b.TurnOff();
				}
			}
		}
		
	}

	[System.NonSerialized]
	public bool slideout = false;
	
	public override void ClickOff(int v){
		heldbutton = -1;
	}	

	int switchesflicked = 0;

	public override void FlickOn (int v)
	{
		if (v == 0){
			switchesflicked += 1;
		}
		if (v == 1){
			switchesflicked += 1;
		}
		if (v == 2){	
			switchesflicked += 1;
		}
		if (switchesflicked >= 3){
			slideout = true;
		}

	}
	public override void FlickOff (int v)
	{
		if (v == 0){
			switchesflicked -= 1;
		}
		if (v == 1){
			switchesflicked -= 1;
		}
		if (v == 2){
			switchesflicked -= 1;
		}

		slideout = false;
	}

	public void Electric()	{
		for (int i = 0; i < EList.Count; i++){
			buttonList[i].OnSound = EList[i];
		}
	}

	public void Normal()	{
		for (int i = 0; i < NList.Count; i++){
			buttonList[i].OnSound = NList[i];
		}
	}

	void Update(){
		base.Update();

		if (currentPower > 0){

			float target = SlideBody.transform.localPosition.y;
			if (slideout){
				target = 0;
			}
			else{
				target = -slideDistance;
			}

			if (SlideBody.transform.localPosition.y < target){
				SlideBody.transform.localPosition += new Vector3(0,slideSpeed,0);
				if (SlideBody.transform.localPosition.y > target){
					SlideBody.transform.localPosition = new Vector3(0,target,0);
				}
			}
			else if (SlideBody.transform.localPosition.y > target){
				SlideBody.transform.localPosition -= new Vector3(0,slideSpeed,0);
				if (SlideBody.transform.localPosition.y < target){
					SlideBody.transform.localPosition = new Vector3(0,target,0);
				}
			}

			for (int i = 0; i < graphList.Count; i++){
				if (heldbutton != i){
					graphList[i].wavelength += ((1 - graphList[i].wavelength) * graphTween) * Time.deltaTime * 60;
					graphList[i].amplitude += ((0 - graphList[i].amplitude) * graphTween) * Time.deltaTime * 60;
					//graphList[i].scrollspeed += ((0 - graphList[i].scrollspeed) * graphTween) * Time.deltaTime * 60;
				}
			}
		}
	}

}
