﻿using UnityEngine;
using System.Collections;

public class Radio_Indicator : MonoBehaviour {
	float x_position;
	RadioPanel rad;

	Vector3 new_pos;

	void Start()	{
		x_position = 0;
		rad = GameObject.Find ("Radio Panel").GetComponent<RadioPanel>();
	}

	void Update()	{
		x_position = (((rad.frequency - 86f) / -3f) - 2.83411f);
		new_pos = transform.localPosition;
		new_pos.x = x_position;
		transform.localPosition = new_pos;
	}
}