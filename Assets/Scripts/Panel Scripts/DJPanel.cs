﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class DJPanel : Panel {

	public AudioClip scratchup;
	public AudioClip scratchdown;

	public GameObject SlideBody;

	public float slideDistance;
	public float slideSpeed;

	public float pitchChangeSpeed;
	public float pitchChangeSpeed2;

	public Vector2 pitchRange;
	public float pitchTween;

	public AudioClip[] drumLoops;
	public GameObject buttonPanel;
	List<ButtonScript> buttonList = new List<ButtonScript>();

	TurnTableAnim ttAnim;

	void Start(){
		base.Start();

		ttAnim = GameObject.Find ("Turntable Animation").GetComponent<TurnTableAnim>();

		Vector3 t = SlideBody.transform.localPosition;
		t.y -= slideDistance;
		SlideBody.transform.localPosition = t;

		audio.Stop ();

		foreach (Transform c in buttonPanel.transform){
			if (c.GetComponent<ButtonScript>())
				buttonList.Add (c.GetComponent<ButtonScript>());
		}
	}

	int heldbutton = -1;

	public override void FlickOff(int v){
		if (v == 0){
			slideout = true;
		}
	}
	public override void FlickOn(int v){
		if (v == 0){
			slideout = false;
			
			foreach (ButtonScript b in buttonList){
				b.TurnOff();
			}
		}
	}

	public override void ClickOn(int v){

		if (v > 0){
			foreach (ButtonScript b in buttonList){
				if (b.sendVal != v)
					b.TurnOff();
			}
			audio.clip = drumLoops[(v - 1)];
			audio.Play ();
		}
	}

	[System.NonSerialized]
	public bool slideout = false;
	
	public override void ClickOff(int v){
		if (v == 0){
			slideout = false;

			foreach (ButtonScript b in buttonList){
				b.TurnOff();
			}
		}
		if (v == 1){
			audio.Stop ();
		}
		if (v == 2){
			audio.Stop ();
		}
		if (v == 3){
			audio.Stop ();
		}
		if (v == 4){
			audio.Stop ();
		}
	}	

	float prev = 0;
	public override void CrankInput(Vector2 v){
		if (v.x == 0){
			if (prev <= 0 && v.y > .3f){
				audio.PlayOneShot (scratchup);
				prev = v.y;
			}
			if (prev >= 0 && v.y < -.3f){
				audio.PlayOneShot (scratchdown);
				prev = v.y;
			}
			audio.pitch += v.y * pitchChangeSpeed;

			if (audio.pitch < pitchRange.x)
				audio.pitch = pitchRange.x;
			if (audio.pitch > pitchRange.y)
				audio.pitch = pitchRange.y;

		}
		if (v.x == 1){
			if (prev <= 0 && v.y > .3f){
				audio.PlayOneShot (scratchup);
				prev = v.y;
			}
			if (prev >= 0 && v.y < -.3f){
				audio.PlayOneShot (scratchdown);
				prev = v.y;
			}
			audio.pitch += v.y * pitchChangeSpeed2;;
			
			if (audio.pitch < pitchRange.x)
				audio.pitch = pitchRange.x;
			if (audio.pitch > pitchRange.y)
				audio.pitch = pitchRange.y;
			
		}
	}


	float openCount = 0f;
	void Update(){
		base.Update();
		if (currentPower > 0){

			audio.pitch += ((1 - audio.pitch) * pitchTween) * Time.deltaTime * 60;

			float target = SlideBody.transform.localPosition.y;
			if (slideout){
				openCount += Time.deltaTime;

				if (openCount > .6f){
					target = 0;
				}

				ttAnim.Open ();
			}
			else{
				target = -slideDistance;
			}

			if (SlideBody.transform.localPosition.y < target){
				SlideBody.transform.localPosition += new Vector3(0,slideSpeed,0);
				if (SlideBody.transform.localPosition.y > target){
					SlideBody.transform.localPosition = new Vector3(1.960661f,target,-0.6496992f);
					ttAnim.Close();
					openCount = 0;
				}
			}
			else if (SlideBody.transform.localPosition.y > target){
				SlideBody.transform.localPosition -= new Vector3(0,slideSpeed,0);
				if (SlideBody.transform.localPosition.y < target){
					SlideBody.transform.localPosition = new Vector3(1.960661f,target,-0.6496992f);

				}
			}

		}
	}

}
