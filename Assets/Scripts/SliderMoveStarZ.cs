﻿using UnityEngine;
using System.Collections;

public class SliderMoveStarZ : MonoBehaviour {

	public GameObject StarController;
	
	public float Mult;
	
	SpaceStarSpawner spacestarspawner;
	Slider slider;
	void Start(){
		spacestarspawner = StarController.GetComponent<SpaceStarSpawner>();
		slider = GetComponent<Slider>();
	}

	void Update () {
		spacestarspawner.directionSpeed.z = slider.GetValue () * Mult;

	}
}
