﻿using UnityEngine;
using System.Collections;

public class Asteroid : MonoBehaviour {

	public int numberOfClumps;
	public int possibleDistanceOfClumps;
	public int possibleDistanceOfAsteroids;
	public GameObject asteroid;
	GameObject[] clumps;

	// Use this for initialization
	void Start () {

		//create clumps
		clumps= new GameObject[numberOfClumps];
		for(int i = 0; i < clumps.Length; i++)
		{
			//create empty holder for clumps
			GameObject aClump = new GameObject("Clump");

			//parent
			aClump.transform.parent = transform;

			//create a random location
			Vector3 position = new Vector3((Random.Range(transform.position.x - possibleDistanceOfClumps * -1, transform.position.x + possibleDistanceOfClumps)), (Random.Range(transform.position.y - possibleDistanceOfClumps * -1, transform.position.y + possibleDistanceOfClumps)), (Random.Range(transform.position.z - possibleDistanceOfClumps * -1, transform.position.z + possibleDistanceOfClumps)));
			aClump.transform.position = position;

			//how many asteroids in clump(magic range number until we know what we want
			int numberOfAsteroidsInClump = Random.Range (1, 4);

			//create asteroid and random locations for those around clump
			for(int j=0; j < numberOfAsteroidsInClump; j++)
			{
				//random location in range from clump
				Vector3 astPosition = new Vector3((Random.Range(aClump.transform.position.x + possibleDistanceOfAsteroids * -1, aClump.transform.position.x + possibleDistanceOfAsteroids)),(Random.Range(aClump.transform.position.y + possibleDistanceOfAsteroids * -1, aClump.transform.position.y + possibleDistanceOfAsteroids)),(Random.Range(aClump.transform.position.z + possibleDistanceOfAsteroids * -1, aClump.transform.position.z + possibleDistanceOfAsteroids)));
				GameObject tempAsteroid = Instantiate(asteroid, astPosition, Quaternion.Euler (0, 0, Random.Range(0, 360))) as GameObject;

				//parent
				tempAsteroid.transform.parent = aClump.transform;
			}


			clumps[i] = aClump;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
