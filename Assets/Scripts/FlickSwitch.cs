using UnityEngine;
using System.Collections;

public class FlickSwitch : MonoBehaviour {

	public GameObject rotatePoint;
	public GameObject switchBody;

	public float rotateRange;

	public float inBetweenTurnMult = 1f;

	public string slideAxis = "x";
	public float flickThreshold;

	bool offon = false;

	public bool startValue;
	
	
	public float MoveTween;
	
	public float MaxStep;
	
	public GameObject PANEL;
	public int SendVal;

	public AudioClip flickOnSound;
	public AudioClip flickOffSound;
	
	float VALUE;
	
	bool held = false;
	
	Vector3 startpos;
	
	float MaxX;
	float MinX;
	float MaxY;
	float MinY;
	
	GameObject cursorpoint;
	MouseFollow MF;
	void Start () {
		switchBody.transform.parent = rotatePoint.transform;
		offon = startValue;
		if (offon){
			rotatePoint.transform.localEulerAngles = new Vector3(rotateRange,0,0);
		}
		else{
			rotatePoint.transform.localEulerAngles = new Vector3(-rotateRange,0,0);
		}

	}

	
	GameObject cursor;
	void Touched(GameObject g){
		cursor = g;
		held = true;
		MF = cursor.GetComponent<MouseFollow>();
		cursorpoint = MF.GetComponent<MouseFollow>().MyPoint;
	}
	
	public float GetValue(){
		return VALUE;
	}

	public void ResetPos(){
		if (offon != startValue){
			if (flickOnSound){
				audio.PlayOneShot (flickOnSound);
			}
		}

		offon = startValue;
		if (offon){
			rotatePoint.transform.localEulerAngles = new Vector3(rotateRange,0,0);
			PANEL.SendMessage ("FlickOn", (SendVal),SendMessageOptions.DontRequireReceiver);

		}
		else{
			rotatePoint.transform.localEulerAngles = new Vector3(-rotateRange,0,0);
			PANEL.SendMessage ("FlickOff", (SendVal),SendMessageOptions.DontRequireReceiver);

		}

	}

	public void UnResetPos(){
		if (offon != startValue){
			if (flickOnSound){
				audio.PlayOneShot (flickOnSound);
			}
		}
		
		offon = startValue;
		if (offon){
			rotatePoint.transform.localEulerAngles = new Vector3(-rotateRange,0,0);
			PANEL.SendMessage ("FlickOff", (SendVal),SendMessageOptions.DontRequireReceiver);
			
		}
		else{
			rotatePoint.transform.localEulerAngles = new Vector3(rotateRange,0,0);
			PANEL.SendMessage ("FlickOn", (SendVal),SendMessageOptions.DontRequireReceiver);
			
		}
		
	}

	void FlickOn(){
		if (!offon){
			offon = true;
			if (PANEL){
				PANEL.SendMessage ("FlickOn", (SendVal),SendMessageOptions.DontRequireReceiver);
			}
			if (flickOnSound){
				audio.PlayOneShot (flickOnSound);
			}
		}
	}

	void FlickOff(){
		if (offon){
			offon = false;
			if (PANEL){
				PANEL.SendMessage ("FlickOff", (SendVal),SendMessageOptions.DontRequireReceiver);
			}
			if (flickOffSound){
				audio.PlayOneShot (flickOffSound);
			}
		}
	}


	void Update () {
		if (held){
			Vector3 destination = MF.lineendpoint + (transform.position - cursor.transform.position);//Camera.main.ScreenToWorldPoint(target) - cursor.transform.localPosition;
			Vector3 dest = MF.lineendpoint - MF.MyPoint.transform.position;
			float d = 0;
			if (slideAxis == "x"){
				//d = destination.x - transform.position.x;
				if (Mathf.Abs (dest.x)> Mathf.Abs (dest.z))
					d = dest.x;
				else
					d = dest.z;
			}
			if (slideAxis == "y"){
				//d = destination.y - transform.position.y;
				d = -dest.y;
			}

			if (d > flickThreshold){
				FlickOff();
			}
			else if (d < -flickThreshold){
				FlickOn();
			}

			float turn = -(d / flickThreshold) * (rotateRange * inBetweenTurnMult);

			if (offon){
				if (turn > 0){
					turn = 0;
				}
				rotatePoint.transform.localEulerAngles = new Vector3(rotateRange + turn,0,0);
			}
			else{
				if (turn < 0){
					turn = 0;
				}
				rotatePoint.transform.localEulerAngles = new Vector3(-rotateRange + turn ,0,0);
			}

			if (Input.GetButtonUp("Fire1")){
				held = false;
			}
		}
		else{
			if (offon){
				rotatePoint.transform.localEulerAngles = new Vector3(rotateRange,0,0);
			}
			else{
				rotatePoint.transform.localEulerAngles = new Vector3(-rotateRange ,0,0);
			}
		}
		
	}
}
