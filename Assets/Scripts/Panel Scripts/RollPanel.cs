﻿using UnityEngine;
using System.Collections;

public class RollPanel : MonoBehaviour {


	Vector2 roll = Vector2.zero;
	
	SpaceStarSpawner spacestarspawner;

	void Awake () {
		spacestarspawner = StarController.GetComponent<SpaceStarSpawner>();
	}

	void ClickOn(int v){
		if (v == 0){
			roll.y = -1;
		}
		if (v == 2){
			roll.y = 1;
		}
		if (v == 1){
			roll.x = -1;
		}
		if (v == 3){
			roll.x = 1;
		}
		move = roll * spinspeed;

		spacestarspawner.directionSpeed = move;

	
	}
	void ClickOff(int v){
		if (v == 0){
			roll.y = 0;
		}
		if (v == 2){
			roll.y = 0;
		}
		if (v == 1){
			roll.x = 0;
		}
		if (v == 3){
			roll.x = 0;
		}
		move = roll * spinspeed;

		spacestarspawner.directionSpeed = move;

	}

	public GameObject StarController;
	
 	Vector2 move;

	public float spinspeed;
	void Update () {
	}
}
