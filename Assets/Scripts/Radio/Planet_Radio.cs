﻿using UnityEngine;
using System.Collections;

public class Planet_Radio : MonoBehaviour {
	public float distance;
	public float max_distance = 10000.0f;
	public float frequency1;
	public float frequency2;
	public float frequency3;
	public float temp_freq1;
	public float temp_freq2;
	public float temp_freq3;
	bool playing = false;

	public AudioClip song1;
	public AudioClip song2;
	public AudioClip song3;

	public GameObject station_a;
	public GameObject station_b;
	public GameObject station_c;

	GameObject ship;
	public GameObject rad_panel;
	public RadioPanel rad;
	void Start()	{
		ship = GameObject.Find ("SHIP");
		rad = GameObject.Find ("Radio Panel").GetComponent<RadioPanel>();
		if(Application.loadedLevelName == "buttons")	{
			station_a = this.transform.GetChild (2).gameObject;
			station_b = this.transform.GetChild (1).gameObject;
			station_c = this.transform.GetChild (3).gameObject;
			station_a.audio.clip = song1;
			station_b.audio.clip = song2;
			station_c.audio.clip = song3;
			station_a.audio.volume = 0.0f;
			station_a.audio.panLevel = 0;
			station_a.audio.mute = true;
			station_a.audio.loop = true;
			station_b.audio.volume = 0.0f;
			station_b.audio.panLevel = 0;
			station_b.audio.mute = true;
			station_b.audio.loop = true;
			station_c.audio.volume = 0.0f;
			station_c.audio.panLevel = 0;
			station_c.audio.mute = true;
			station_c.audio.loop = true;
		}
		else if(Application.loadedLevelName == "Mountain Planet" || Application.loadedLevelName == "Water_Planet" || Application.loadedLevelName == "Desert Planet" || Application.loadedLevelName == "Strawberry Planet")	{
			station_a = this.transform.GetChild (2).gameObject;
			station_b = this.transform.GetChild (1).gameObject;
			station_c = this.transform.GetChild (0).gameObject;
			station_a.audio.clip = song1;
			station_b.audio.clip = song2;
			station_c.audio.clip = song3;
			station_a.audio.volume = 0.0f;
			station_a.audio.panLevel = 0;
			station_a.audio.mute = true;
			station_a.audio.loop = true;
			station_b.audio.volume = 0.0f;
			station_b.audio.panLevel = 0;
			station_b.audio.mute = true;
			station_b.audio.loop = true;
			station_c.audio.volume = 0.0f;
			station_c.audio.panLevel = 0;
			station_c.audio.mute = true;
			station_c.audio.loop = true;
			rad = GameObject.Find ("Radio Panel").GetComponent<RadioPanel>();
		}
	}



	void DoRadio(){
		//temp_freq is the "distance" between the radio's frequency and the planet's frequency
		temp_freq1 = Mathf.Abs (frequency1 - rad.frequency);
		temp_freq2 = Mathf.Abs (frequency2 - rad.frequency);
		temp_freq3 = Mathf.Abs (frequency3 - rad.frequency);
		
		if(rad.power == true)	{
			if(distance < max_distance)	{
				if(rad.frequency == frequency1)	{
					station_a.audio.mute = false;
					rad.tuned_in = true;
					station_a.audio.volume = (1.0f - distance/max_distance);
				}
				else if(temp_freq1 < 0.4f){
					station_a.audio.mute = false;
					rad.tuned_in = true;
					station_a.audio.volume = (1.0f - distance/max_distance);
					station_a.audio.volume -= temp_freq1;
				}
				else if(rad.frequency == frequency2)	{
					station_b.audio.mute = false;
					rad.tuned_in = true;
					station_b.audio.volume = (1.0f - distance/max_distance);
				}
				else if(temp_freq2 < 0.4f){
					station_b.audio.mute = false;
					rad.tuned_in = true;
					station_b.audio.volume = (1.0f - distance/max_distance);
					station_b.audio.volume -= temp_freq2;
				}
				else if(rad.frequency == frequency3)	{
					station_c.audio.mute = false;
					rad.tuned_in = true;
					station_c.audio.volume = (1.0f - distance/max_distance);
				}
				else if(temp_freq3 < 0.4f){
					station_c.audio.mute = false;
					rad.tuned_in = true;
					station_c.audio.volume = (1.0f - distance/max_distance);
					station_c.audio.volume -= temp_freq3;
				}
				else{
					rad.tuned_in = false;
					station_a.audio.volume = 0.0f;
					station_b.audio.volume = 0.0f;
					station_a.audio.mute = true;
					station_b.audio.mute = true;
					station_c.audio.volume = 0.0f;
					station_c.audio.mute = true;
				}
			}
			else{
				station_a.audio.volume = 0.0f;
				station_b.audio.volume = 0.0f;
				station_c.audio.volume = 0.0f;
				station_a.audio.mute = true;
				station_b.audio.mute = true;
				station_c.audio.mute = true;
			}
		}
		else{
			station_a.audio.volume = 0.0f;
			station_b.audio.volume = 0.0f;
			station_c.audio.volume = 0.0f;
			station_a.audio.mute = true;
			station_b.audio.mute = true;
			station_c.audio.mute = true;
		}
	}


	void Update () {
		if(station_a.audio.volume > 0.0f)
			rad.channel_volume = station_a.audio.volume;
		if(station_b.audio.volume > 0.0f)
			rad.channel_volume = station_b.audio.volume;
		if(station_c)	{
			if(station_c.audio.volume > 0.0f)
				rad.channel_volume = station_c.audio.volume;
		}

		distance = Vector3.Distance (ship.transform.position, this.transform.position);
		if(playing == false)	{
			station_a.audio.Play ();
			station_b.audio.Play ();
			if(station_c)	
				station_c.audio.Play ();
			playing = true;
		}

		if (rad_panel){
			DoRadio();
		}
		else{
			rad_panel = GameObject.Find ("Radio Panel");
			if (rad_panel)
				rad = rad_panel.GetComponent<RadioPanel>();
		}
	}
}
