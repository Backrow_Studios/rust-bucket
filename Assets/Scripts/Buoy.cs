﻿using UnityEngine;
using System.Collections;

public class Buoy : MonoBehaviour {

	public AnimationCurve curve;
	Vector3 startPos;
	public bool left;

	void Start () {
		startPos = transform.localPosition;
		left = true;
	}
	
	float curveX = 0;

	void Update () {

		curveX += Time.deltaTime;

		if (curveX > 1){
			curveX -= 1;
		}
	
		Vector3 newrot = transform.localRotation.eulerAngles;

		if(transform.localRotation.eulerAngles.z < 352f && transform.localRotation.eulerAngles.z > 340f)
			left = true;
		else if(transform.localRotation.eulerAngles.z > 8f && transform.localRotation.eulerAngles.z < 20f)
			left = false;

		if(left)	{
			newrot.z += (curve.Evaluate (curveX) * 0.5f);
		}
		else {
			newrot.z -= (curve.Evaluate (curveX) * 0.5f);
		}

		transform.localRotation = Quaternion.Euler (newrot.x, newrot.y, newrot.z);

		Vector3 newpos = startPos;

		newpos.y += curve.Evaluate (curveX);
		transform.localPosition = newpos;
	}
}
