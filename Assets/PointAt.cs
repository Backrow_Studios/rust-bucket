﻿using UnityEngine;
using System.Collections;

public class PointAt : MonoBehaviour {

	public GameObject target;
	void Start () {
		transform.rotation = Quaternion.LookRotation (target.transform.position,Vector3.up);
	}

}
