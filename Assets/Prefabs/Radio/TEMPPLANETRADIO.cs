﻿using UnityEngine;
using System.Collections;

public class TEMPPLANETRADIO : MonoBehaviour {
	//for the sake of testing, will make a planet's distance pre-set and static for now
	public float distance;
	public float max_distance = 10000.0f;
	public float frequency1;
	public float frequency2;
	public float temp_freq1;
	public float temp_freq2;
	bool playing = false;
	
	public AudioClip song1;
	public AudioClip song2;
	
	GameObject ship;
	
	GameObject rad_panel;
	RadioPanel rad;
	void Start()	{
		ship = GameObject.Find ("SHIP");
		audio.audio.loop = true;
		audio.panLevel = 0;
		audio.mute = true;
	}
	
	
	
	void DoRadio(){
		//temp_freq is the "distance" between the radio's frequency and the planet's frequency
		temp_freq1 = Mathf.Abs (frequency1 - rad.frequency);
		temp_freq2 = Mathf.Abs (frequency2 - rad.frequency);
		
		if(rad.power == true)	{
			//"station 1"
			if(rad.frequency == frequency1)	{
				audio.clip = song1;
				rad.tuned_in = true;
				if(distance > max_distance)	{
					audio.volume = 0.0f;
				}
				else{
					audio.volume = (1.0f - distance/max_distance);
				}
			}
			else if(temp_freq1 < 0.2f){
				audio.clip = song1;
				rad.tuned_in = true;
				if(distance > max_distance)	{
					audio.volume = 0.0f;
				}
				else{
					audio.volume = (1.0f - distance/max_distance);
					audio.volume -= 0.4f;
				}
			}
			//"station 2"
			else if(rad.frequency == frequency2)	{
				audio.clip = song2;
				rad.tuned_in = true;
				if(distance > max_distance)	{
					audio.volume = 0.0f;
				}
				else{
					audio.volume = (1.0f - distance/max_distance);
				}
			}
			else if(temp_freq2 < 0.2f){
				audio.clip = song2;
				rad.tuned_in = true;
				if(distance > max_distance)	{
					audio.volume = 0.0f;
				}
				else{
					audio.volume = (1.0f - distance/max_distance);
					audio.volume -= 0.4f;
				}
			}
			else{
				audio.volume = 0.0f;
				audio.mute = true;
			}
		}
		else{
			audio.volume = 0.0f;
			audio.mute = true;
		}
		
		
	}
	
	
	void Update () {
		distance = Vector3.Distance (ship.transform.position, this.transform.position);
		if(playing == false)	{
			audio.Play ();
			playing = true;
		}
		audio.mute = false;
		//GameObject rad_panel = GameObject.Find ("Radio Panel");
		
		if (rad_panel){
			DoRadio();
			if(audio.volume > 0)	{
				rad.static_volume = 1.0f - audio.volume;
				Debug.Log (audio.clip);
			}
		}
		else{
			rad_panel = GameObject.Find ("Radio Panel");
			if (rad_panel)
				rad = rad_panel.GetComponent<RadioPanel>();
		}
		
	}
}
