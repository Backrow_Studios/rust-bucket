﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour {

	public GameObject cursor;
	public float cursorXZone;
	public float cursorYTopZone;
	public float cursorYBottomZone;
	public float MaxXTurnSpeed;
	public float xAccel;
	public float xDecelTween = .1f;

	public float maxYTurnSpeed;
	public float yAccel;
	public float yDecelTween;
	public bool passiveScroll = false;

	public GameObject planetHolder;
	public Vector3 myDirection;
	public GameObject dirPointer;


	public float rotateToPlanetTween = .1f;
	public float doneTurningAngle = .1f;
	public float maxTurnStep = 1f;

	public float mouseTurnSpeed = .1f;
	public float maxMouseTurnStep = 1f;

	public float screenShakeFreq = 1f;

	public float rotationXLimitAngle = 60f;
	public float rotationYLimitAngle = 60f;


	Vector3 eul = Vector3.zero;
	MouseFollow mousefollow;
	SpaceStarSpawner spacestarspawner;

	Vector3 startPosition;
	void Start () {
		startPosition = transform.position;

		mousefollow = cursor.GetComponent<MouseFollow>();
		spacestarspawner = GetComponent<SpaceStarSpawner>();
		//eul = planetHolder.transform.eulerAngles;

		//rottowards = planetHolder.transform.rotation;
	}

	void OnLevelWasLoaded(){
		if (Application.loadedLevelName == "buttons"){
			planetHolder = GameObject.Find ("PLANET HOLDER");
			eul = planetHolder.transform.eulerAngles;
			rottowards = planetHolder.transform.rotation;
		}
	}

	float SignedAngleBetween(Vector3 a, Vector3 b, Vector3 n){
		// angle in [0,180]
		float angle = Vector3.Angle(a,b);
		float sign = Mathf.Sign(Vector3.Dot(n,Vector3.Cross(a,b)));
		
		// angle in [-179,180]
		float signed_angle = angle * sign;
		
		// angle in [0,360] (not used but included here for completeness)
		//float angle360 =  (signed_angle + 180) % 360;
		
		return signed_angle;
	}

	float xturnspeed = 0f;
	float yturnspeed = 0f;
	void FollowCursor(){
		RightClickCamera();

		Vector3 onscreenpos = Camera.main.WorldToScreenPoint (mousefollow.lineendpoint);

		if (onscreenpos.x > (Screen.width - (Screen.width * cursorXZone) )){
			xturnspeed += xAccel * Time.deltaTime * 60;
		}
		else if (onscreenpos.x < (0 + (Screen.width * cursorXZone) )){
			xturnspeed -= xAccel * Time.deltaTime * 60;

		}
		else {
			xturnspeed += ((0-xturnspeed)*xDecelTween) * Time.deltaTime * 60;
		}

		if (onscreenpos.y > (Screen.height - (Screen.height * cursorYTopZone) )){
			yturnspeed -= yAccel * Time.deltaTime * 60;
		}
		else if (onscreenpos.y < (0 + (Screen.height * cursorYBottomZone) )){
			yturnspeed += yAccel * Time.deltaTime * 60;
			
		}
		else {
			yturnspeed += ((0-yturnspeed)*yDecelTween) * Time.deltaTime * 60;
		}

		if (Input.GetButton("Fire2")){
			if (xturnspeed > maxMouseTurnStep){
				xturnspeed = maxMouseTurnStep;
			}
			if (xturnspeed < -maxMouseTurnStep){
				xturnspeed = -maxMouseTurnStep;
			}
			
			if (yturnspeed > maxMouseTurnStep){
				yturnspeed = maxMouseTurnStep;
			}
			if (yturnspeed < -maxMouseTurnStep){
				yturnspeed = -maxMouseTurnStep;
			}
		}
		else{
			if (xturnspeed > MaxXTurnSpeed){
				xturnspeed = MaxXTurnSpeed;
			}
			if (xturnspeed < -MaxXTurnSpeed){
				xturnspeed = -MaxXTurnSpeed;
			}
			
			if (yturnspeed > maxYTurnSpeed){
				yturnspeed = maxYTurnSpeed;
			}
			if (yturnspeed < -maxYTurnSpeed){
				yturnspeed = -maxYTurnSpeed;
			}
		}
		
		Vector3 lastpos = Camera.main.WorldToScreenPoint(cursor.transform.position);


		RotationBounds();
		transform.Rotate (Vector3.up,Time.deltaTime * 60 * xturnspeed,Space.World);
		transform.Rotate (transform.right,Time.deltaTime * 60 * yturnspeed,Space.World);


		if (!passiveScroll){
			if (Input.GetButton("Fire2") && !Input.GetButton("Fire1"))	
				cursor.transform.position = Camera.main.ScreenToWorldPoint(lastpos);
			else
				mousefollow.MovePointer (Camera.main.WorldToScreenPoint(cursor.transform.position) - lastpos);
		}
	}

	void RotationBounds(){
	
		if ((SignedAngleBetween(transform.rotation * Vector3.forward, Vector3.forward, Vector3.up)) > rotationXLimitAngle){
			if (xturnspeed < 0){
				xturnspeed = 0;
			}
		}
		if ((SignedAngleBetween(transform.rotation * Vector3.forward, Vector3.forward, Vector3.up)) < -rotationXLimitAngle){
			if (xturnspeed > 0){
				xturnspeed = 0;
			}
		}

		if (Mathf.Abs(SignedAngleBetween(transform.rotation * Vector3.forward, Vector3.up, Vector3.forward)) < rotationYLimitAngle){
			if (yturnspeed < 0){
				yturnspeed = 0;
			}
		}
		if (Mathf.Abs(SignedAngleBetween(transform.rotation * Vector3.forward, -Vector3.up, Vector3.forward)) < rotationYLimitAngle){
			if (yturnspeed > 0){
				yturnspeed = 0;
			}
		}

	}

	void RightClickCamera(){
		if (Input.GetButton("Fire2") && !Input.GetButton("Fire1")){

			xturnspeed += Input.GetAxis("Mouse X") * mouseTurnSpeed;
			yturnspeed -= Input.GetAxis("Mouse Y") * mouseTurnSpeed;

		}

	}

	Quaternion rottowards;
	public void RotateShipTowards(Quaternion q){
		rottowards = q;
	}

	float shakeamount = 0;
	public float ShakeAmount{
		get{
			return shakeamount;
		}
		set{
			shakeamount = value;
		}
	}

	float shakecount = 0;
	void ScreenShake(){
		shakecount += Time.deltaTime;
		if (shakecount > screenShakeFreq){
			transform.position = startPosition + new Vector3(Random.Range(-shakeamount,shakeamount),Random.Range(-shakeamount,shakeamount),Random.Range(-shakeamount,shakeamount));
			shakecount = 0;
		}
	}

	void Update () {

		FollowCursor();
		ScreenShake();

		Vector3 n = new Vector3(spacestarspawner.directionSpeed.x,spacestarspawner.directionSpeed.y,0);

		if (n.x != 0)
			rottowards = Quaternion.RotateTowards(rottowards,rottowards * Quaternion.Euler(new Vector3(0, 90, 0)),n.x);
		if (n.y != 0)
			rottowards = Quaternion.RotateTowards(rottowards,rottowards * Quaternion.Euler(new Vector3(90, 0, 0)),n.y);

		if (planetHolder){
			float step = Quaternion.Angle (planetHolder.transform.rotation,rottowards);
		
			if (step > doneTurningAngle){
				spacestarspawner.turning = true;
			}
			else{
				spacestarspawner.turning = false;
			}

			step = step * rotateToPlanetTween;
			if (step > maxTurnStep){
				step = maxTurnStep;
			}

			step = step * Time.deltaTime * 60;
			planetHolder.transform.rotation = Quaternion.RotateTowards (planetHolder.transform.rotation,rottowards,step);

			float move = spacestarspawner.directionSpeed.z;

			Vector3 currentVelocity = dirPointer.transform.forward * move; 

			foreach (Transform child in planetHolder.transform){
				child.position += currentVelocity * Time.deltaTime * 60;
			}
		}

	}
}
