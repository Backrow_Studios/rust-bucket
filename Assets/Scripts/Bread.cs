﻿using UnityEngine;
using System.Collections;

public class Bread : MonoBehaviour {
	public GameObject Body;

	public Material myTexture = null;

	public Material[] mats;

	[System.NonSerialized]
	public Collider ignoreCol;

	public AudioClip GrabSound;
	public AudioClip HitSound;

	public float followtween;
	public float floatAboveAmount = .1f;
	
	public Vector3 heldBodyPos;
	public float heldTween;
	public float rotateTween;

	[System.NonSerialized]
	public bool readyToDelete = false;

	AnalysisPanel analysisPanel;

	public string name = "TOASTED BREAD";
	public string description = "Hey, right on. Everybody likes toast.";
	
	void Start () {
		Physics.IgnoreLayerCollision(9,0);
		Physics.IgnoreLayerCollision(9,1);

		int r = Random.Range (0,mats.Length);

		if (Body.renderer)
			Body.renderer.material = mats[r];

		analysisPanel = GameObject.Find ("AnalysisPanel").GetComponent<AnalysisPanel>();

	}

	public float collisionSoundThreshold = 4;

	void OnCollisionEnter(Collision col){
		if(!rigidbody.isKinematic){
			if (rigidbody.velocity.magnitude > collisionSoundThreshold){
				audio.PlayOneShot(HitSound);
			}	
			if (col.gameObject.name == "HoloEnd"){
				if (readyToDelete){
					analysisPanel.NewData(myTexture,name,description);
					Destroy (gameObject);
				}
			}
		}
		
	}

	public float releasePow;
	public Vector3 releaseDir;

	void Release(Vector3 force){
		held = false;
		rigidbody.useGravity = true;
		rigidbody.freezeRotation = false;
		rigidbody.isKinematic = false;
		rigidbody.constraints = RigidbodyConstraints.None;

		if (releasePow > 0){
			Vector3 vec = releaseDir;
			vec = transform.rotation * vec;
			rigidbody.velocity = (force /2) + (vec * releasePow);
		}
		else{
			rigidbody.velocity = force;
		}
	}

	[System.NonSerialized]
	public bool ignoring = false;

	void Update () {
		if (ignoring){
			if (rigidbody.velocity.y <= 0){
				Physics.IgnoreCollision(ignoreCol,collider,false);
				ignoring = false;
			}
		}

		if (held){
			rigidbody.useGravity = false;
			rigidbody.isKinematic = true;
			rigidbody.freezeRotation = true;
			
			transform.position = cursor.transform.position; //+ dif;
			transform.position -= (Camera.main.transform.position-cursor.transform.position) * floatAboveAmount;
			
			Quaternion targetrot = cursor.transform.rotation;
			
			float angle = Quaternion.Angle(transform.rotation,targetrot);
			float step = angle * rotateTween;
			
			transform.rotation = Quaternion.RotateTowards(transform.rotation,targetrot,step * Time.deltaTime * 60);

			Body.transform.localPosition += ((heldBodyPos - Body.transform.localPosition)*heldTween) * Time.deltaTime * 60;
			
		}
		else{
			Body.transform.localPosition += ((Vector3.zero - Body.transform.localPosition)*heldTween) * Time.deltaTime * 60;
		}
		if (Input.GetButtonUp("Fire1") && held){
			Release (Vector3.zero);
		}
		
	}
	
	Vector3 dif;
	bool held = false;
	GameObject cursor;
	void Touched(GameObject g){
		cursor = g;
		held = true;
		audio.PlayOneShot(GrabSound);
		
		//dif = transform.position - cursor.transform.position;
		//dif.z = 0;
	}
}
