﻿using UnityEngine;
using System.Collections;

public class AlwaysFaceCamera : MonoBehaviour {

	Camera cam;
	Vector3 updir = Vector3.up;

	void Start(){
		cam = Camera.main;
	}

	public void SetUpDir(Vector3 d){
		updir = d;
	}

	void LateUpdate () {
		transform.rotation = Quaternion.LookRotation (cam.transform.position - transform.position,updir);
		//transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.back, Camera.main.transform.rotation * Vector3.up); 
	}
}
