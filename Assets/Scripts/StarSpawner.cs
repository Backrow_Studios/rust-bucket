﻿using UnityEngine;
using System.Collections;

public class StarSpawner : MonoBehaviour {
	public GameObject star;
	//var for x bounds ( -110 - 110)
	public int leftBound = -110;
	public int rightBound = 110;

	//var for z bounds (45 - 0)
	public int farZBound = 45;
	public int closeZBound = -9;

	//var for height bounds (-43 - 43)
	public int lowerBound = -43;
	public int upperBound = 43;

	public int howManyStars;

	public Vector3 directionSpeed;

	public GameObject parent;

	// Use this for initialization
	void Start () {
		for(int i = 0; i < howManyStars; i++) 
		{
			//Spawn in random position
			float randx = Random.Range(leftBound,rightBound);
			float randy = Random.Range(lowerBound, upperBound);
			float randz = Random.Range (farZBound, closeZBound);
			GameObject g = (GameObject)Instantiate(star, new Vector3(randx,randy,randz), Quaternion.identity);
			g.transform.parent = parent.transform;
		}
	}
	
	// Update is called once per frame
	void Update () {

	}
}
