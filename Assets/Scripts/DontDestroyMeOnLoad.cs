﻿using UnityEngine;
using System.Collections;

public class DontDestroyMeOnLoad : MonoBehaviour {
	public bool beginning = true;

	void Start () {
		DontDestroyOnLoad(gameObject);
		if (Application.loadedLevelName == "ShipStart"){
			Application.LoadLevel ("buttons");
		}
	}

	void OnLevelWasLoaded(){
		if (Application.loadedLevelName == "buttons"){
			beginning = false;
		}
	}

}
