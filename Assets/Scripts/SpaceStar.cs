﻿using UnityEngine;
using System.Collections;

public class SpaceStar : MonoBehaviour {

	public Object starBody;
	AlwaysFaceCamera starFaceScript;

	GameObject starbody;

	public float distanceFromCamera;
	public float graphicTween = .5f;
	public int sizeMultiplier;
	float currentMaxDistance = 100f;

	[System.NonSerialized]
	public Vector2 outsideCamRange;// = Vector2.zero;

	public float spawnEdgeVariance = 1f;

	public float turningGenDistanceRange = 5f;

	public float appearTween;

	Vector3 origionalScale;

	public AnimationCurve distancechange;

	float distance = 0;

	Camera Cam;
	Transform starCenter;
	SpaceStarSpawner spacestarspawner;

	public float stretchMult;

	void Start () {

		checkcount = Random.Range (1,6);

		spacestarspawner = Camera.main.GetComponent<SpaceStarSpawner>();

		Cam = GameObject.FindWithTag ("StarCamera").camera;
		starCenter = GameObject.Find("Star Center").transform;

		starbody = (GameObject)Instantiate (starBody);
		//starbody.transform.parent = transform;
		starbody.transform.localPosition = Vector3.zero;
		starbody.transform.localEulerAngles = Vector3.zero;

		starFaceScript = starbody.GetComponent<AlwaysFaceCamera>();

		//set origional scale
		origionalScale = starbody.transform.localScale;
		currentMaxDistance = Vector3.Distance(transform.position,Cam.transform.position);
	}

	int checkcount;
	float curcheckcount = 0;
	bool m = false;

	bool TryCheck(){
		curcheckcount += 1;
		if (curcheckcount == checkcount){
			m = CheckOffScreen();
		}
		if (curcheckcount >= 6){
			curcheckcount = 0;
		}

		return m;
	}

	bool CheckOffScreen(){
		bool moved = false;
		
		Vector3 min = Cam.ScreenToWorldPoint(new Vector3(-outsideCamRange.x, -outsideCamRange.y, Cam.WorldToScreenPoint(transform.position).z));
		Vector3 max = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth + outsideCamRange.x, Cam.pixelHeight + outsideCamRange.y,  Cam.WorldToScreenPoint(transform.position).z));

		Vector3 move = spacestarspawner.directionSpeed;
		bool turning = spacestarspawner.turning;
		bool forwardmove = false;

		if (move.z != 0){
			forwardmove = true;
			turning = false;
		}




		if (transform.position.x < min.x){
			if (turning)
				transform.position = RandomOnRightEdge();
			else if (forwardmove)
				transform.position = ForwardRandom();
			moved = true;

		}
		else if (transform.position.x > max.x){
			if (turning)
				transform.position = RandomOnLeftEdge();
			else if (forwardmove)
				transform.position = ForwardRandom();
			moved = true;
		}
		if (transform.position.y < min.y){
			if (turning)
				transform.position = RandomOnBottomEdge();
			else if (forwardmove)
				transform.position = ForwardRandom();
			moved = true;
		}
		else if (transform.position.y > max.y){
			if (turning)
				transform.position = RandomOnTopEdge();
			else if (forwardmove)
				transform.position = ForwardRandom();
			moved = true;
		}
		return moved;
	}

	Vector3 bodytarget;
	float sizeScaler;
	Vector3 sizeVector;
	float scalemult = 1;

	void Update () {

		Vector3 bodytarget = Cam.transform.position + (transform.position - Cam.transform.position).normalized * distanceFromCamera;

		bool moved = TryCheck();

		if (moved){
			bodytarget = Cam.transform.position + (transform.position - Cam.transform.position).normalized * distanceFromCamera;
			starbody.transform.position =bodytarget;
			if (spacestarspawner.directionSpeed.z != 0){
				Respawned();
			}
			else {
				TurnRespawned();
			}

		}

		//scale to size
		//distance = Vector3.Distance(transform.position,Cam.transform.position);
		distance = Mathf.Abs(transform.position.z - Cam.transform.position.z);


		sizeScaler = 1 - (distance/currentMaxDistance);
		sizeScaler = distancechange.Evaluate(sizeScaler);

		sizeVector = Vector3.one * sizeScaler;
		sizeVector = sizeVector * sizeMultiplier;

		scalemult += ((1 - scalemult) * appearTween) * Time.deltaTime * 60;

		float speedmult = Mathf.Abs(spacestarspawner.directionSpeed.z);
		speedmult = speedmult * stretchMult;

		if (speedmult < 1){
			speedmult = 1;
		}
		sizeVector.y = sizeVector.y * speedmult;

		starbody.transform.localScale = (origionalScale + sizeVector) * scalemult;
		
		starbody.transform.position += ((bodytarget - starbody.transform.position) * graphicTween) * Time.deltaTime * 60;
		starFaceScript.SetUpDir (starCenter.position - starbody.transform.position);
	}

	Vector3 RandomOnRightEdge(){
		float screenX = Cam.pixelWidth + outsideCamRange.x + Random.Range(-spawnEdgeVariance,0);
		float screenY = Random.Range(-outsideCamRange.y, Cam.pixelHeight + outsideCamRange.y);
		float ScreenZ = Random.Range(10, Cam.farClipPlane);

		Vector3 point = Cam.ScreenToWorldPoint(new Vector3(screenX, screenY, ScreenZ));

		return point;
	}
	Vector3 RandomOnLeftEdge(){
		float screenX = -outsideCamRange.x + Random.Range(0,spawnEdgeVariance);
		float screenY = Random.Range(-outsideCamRange.y, Cam.pixelHeight + outsideCamRange.y);
		float ScreenZ = Random.Range(10, Cam.farClipPlane);

		Vector3 point = Cam.ScreenToWorldPoint(new Vector3(screenX, screenY, ScreenZ));

		return point;
	}
	Vector3 RandomOnTopEdge(){
		float screenX = Random.Range(-outsideCamRange.x, Cam.pixelWidth + outsideCamRange.x);
		float screenY = -outsideCamRange.y + Random.Range(0,spawnEdgeVariance);
		float ScreenZ = Random.Range(10, Cam.farClipPlane);

		Vector3 point = Cam.ScreenToWorldPoint(new Vector3(screenX, screenY, ScreenZ));

		return point;
	}
	Vector3 RandomOnBottomEdge(){
		float screenX = Random.Range(-outsideCamRange.x, Cam.pixelWidth + outsideCamRange.x);
		float screenY = Cam.pixelHeight + outsideCamRange.y + Random.Range(-spawnEdgeVariance,0);
		float ScreenZ = Random.Range(10, Cam.farClipPlane);

		Vector3 point = Cam.ScreenToWorldPoint(new Vector3(screenX, screenY, ScreenZ));

		return point;
	}
	Vector3 ForwardRandom(){
		float screenX = Random.Range(-outsideCamRange.x, Cam.pixelWidth + outsideCamRange.x);
		float screenY = Random.Range(-outsideCamRange.y, Cam.pixelHeight + outsideCamRange.y);
		float ScreenZ = Random.Range(10, Cam.farClipPlane);

		Vector3 point = Cam.ScreenToWorldPoint(new Vector3(screenX, screenY, ScreenZ));

		return point;
	}
	void Respawned(){
		//distance = Vector3.Distance(transform.position,Cam.transform.position);
		distance = Mathf.Abs(transform.position.z - Cam.transform.position.z);
		currentMaxDistance = distance;
		scalemult = 0;

	}
	void TurnRespawned(){
		distance = Mathf.Abs(transform.position.z - Cam.transform.position.z);

		distance = Vector3.Distance(transform.position,Cam.transform.position);
		currentMaxDistance = distance + Random.Range (0,turningGenDistanceRange);
	}
}
