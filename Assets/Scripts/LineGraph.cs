﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineGraph : MonoBehaviour {

	LineRenderer myLineRenderer;

	public bool sineWave = false;

	List<Transform> PointList;
	public int howManyPoints;
	public float scrollspeed;
	public float amplitude;
	float leftBound;
	float rightBound;
	float zEdgeOfGraph;

	public AudioClip pingSound;
	bool ping = false;

	//TODO ability to change graph by pressing buttons
	// Use this for initialization
	void Start () {
		myLineRenderer = gameObject.GetComponent<LineRenderer>();

		leftBound = -transform.localScale.x / 2;
		rightBound = transform.localScale.x / 2;
		zEdgeOfGraph = 0;
		//Debug.Log ("left: " + leftBound + " right: " + rightBound + " edge: " + zEdgeOfGraph);

		PointList = new List<Transform>();
		for(int i = 0; i < howManyPoints; i++) {
			//TODO add "warmup" period
			GameObject newGO = new GameObject();
			Vector3 newPoint = new Vector3(transform.position.x + (i * .05f), transform.position.y + (Mathf.Sin(i))/3, zEdgeOfGraph - .05f);
			newGO.transform.position = newPoint;
			newGO.transform.parent = transform;
			PointList.Add(newGO.transform);
		}
		myLineRenderer.SetVertexCount(howManyPoints);




	}

	Vector3 tempPoint = Vector3.zero;

	void MovePoints(){
		//move points
		for (int g = 0; g < PointList.Count; g++){
			PointList[g].position += -transform.right * scrollspeed;
			if(PointList[g].position.x < leftBound)//pop off front of list and add to end of list
			{
				tempPoint = new Vector3(rightBound, PointList[g].position.y, PointList[g].position.z);
				//PointList.RemoveAt(0);
				//PointList.Add(tempPoint);
				PointList[0].position = tempPoint;
				//Debug.Log ("removed: " + tempPoint);
			}
		}
	}

	float p;
	public float wavelength = 1;
	void SinPoints(){

		p += scrollspeed * Time.deltaTime * 60;

		if (p > 360){
			p = 0;
		}

		for (float i = 0; i < PointList.Count; i++){
			tempPoint.x = ((i / howManyPoints)) * Mathf.Abs (rightBound- leftBound);
			tempPoint.y = ((Mathf.Sin((i/wavelength) + p))/3) * amplitude;
			tempPoint.z = zEdgeOfGraph / 3; 
			PointList[(int)i].localPosition = tempPoint;
		}

		if (pingSound){
			if (PointList[0].localPosition.y > .05f){
				if (!ping){
					ping = true;
					audio.PlayOneShot (pingSound);
				}
			}
			else{
				ping = false;
			}
		}
	}

	// Update is called once per frame
	void Update () {



		if (sineWave){
			leftBound = -transform.localScale.x / 2;
			rightBound = transform.localScale.x / 2;
			zEdgeOfGraph = 0;
			SinPoints();
		}
		else{
			MovePoints();
		}



		for (int i = 0; i < PointList.Count; i++){
			myLineRenderer.SetPosition(i,PointList[i].position);
		}

	}
}
