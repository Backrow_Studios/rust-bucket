﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class TakeOffPanel : Panel	{

	public AudioClip errorSound;

	float onLightIntensity;
	int switch_count;

	public List<GameObject> arrowObjs = new List<GameObject>();
	List<Material> arrowMats = new List<Material>();
	List<FlickSwitch> switchList = new List<FlickSwitch>();

	MovePanel movepanelScript;

	public Color arrowOff;
	public Color arrowOn;

	void Start(){
		switch_count = 0;
		base.Start();

		movepanelScript = GameObject.Find ("MovePanel").GetComponent<MovePanel>();

		foreach (Transform c in transform){
			if(c.GetComponent<FlickSwitch>()){
				switchList.Add (c.GetComponent<FlickSwitch>());
			}
		}

		for (int i = 0; i < arrowObjs.Count; i++){
			arrowMats.Add(arrowObjs[i].renderer.material);
			arrowMats[i].color = arrowOff;
		}
	}
	[System.NonSerialized]
	public bool onplanet = false;

	[System.NonSerialized]
	public TakeOff takeoffscript;
	void OnLevelWasLoaded(){
		GameObject t = GameObject.Find ("Scenary");
		if (t){
			takeoffscript = t.GetComponent<TakeOff>();
			onplanet = true;
		}
		else{
			onplanet = false;
		}
	}
	public override void FlickOn (int v)
	{
		//some are pos/neg because the switches need to all be flicked facing inwards
		if (v == 0){
			switch_count -= 1;
			arrowMats[v].color = arrowOff;

		}
		if (v == 1){
			switch_count -= 1;
			arrowMats[v].color = arrowOff;

		}
		if (v == 2){
			switch_count += 1;
			arrowMats[v].color = arrowOn;


		}
		if (v == 3){
			switch_count += 1;
			arrowMats[v].color = arrowOn;

		}

		if (switch_count < 0){
			switch_count = 0;
		}
		if (switch_count > 4){
			switch_count = 4;
		}

	}
	public override void FlickOff (int v)
	{
		if (v == 0){
			switch_count += 1;
			arrowMats[v].color = arrowOn;

		}
		if (v == 1){
			switch_count += 1;
			arrowMats[v].color = arrowOn;

		}
		if (v == 2){
			switch_count -= 1;
			arrowMats[v].color = arrowOff;

		}
		if (v == 3){
			switch_count -= 1;
			arrowMats[v].color = arrowOff;

		}


		if (switch_count < 0){
			switch_count = 0;
		}
		if (switch_count > 4){
			switch_count = 4;
		}
	}

	void Update(){
		base.Update();
	}

	public override void ClickOn(int v){
		if (v == 0){

		}
		
	}
	public override void ClickOff(int v){
		if (v == 0){
			if (currentPower > .03f){
				if (takeoffscript){
					if(switch_count == 4){
						if(GameObject.Find ("Scenary").GetComponent<Land>().landed == true){
							takeoffscript.launch = true;
						}
					}
					else{
						audio.PlayOneShot (errorSound);
					}
				}				
				
				int tempcount = switch_count;

				foreach (FlickSwitch f in switchList){
					f.ResetPos();
				}
				
				if (movepanelScript){
					movepanelScript.landcount = tempcount;
				}
				
				
				
				
			}
		}
	}	
}
