﻿using UnityEngine;
using System.Collections;

public class ToastPanel : Panel {

	Transform shipTransform;
	public GameObject toaster;
	public GameObject[] toastSpawners;

	public Object toast;

	public float toastBoost = 4;

	public GameObject lever;
	Pullcord leverScript;
	public float toastCookTime = 2;
	float cookCount;

	public AudioClip downSound;
	public AudioClip dingSound;
	public AudioClip popSound;

	[System.NonSerialized]
	public int totalToastsCreated = 0;

	void Start(){
		base.Start ();
		leverScript = lever.GetComponent<Pullcord>();
		shipTransform = GameObject.Find ("SHIP").transform;

	}

	float leverInput = 0;

	public override void SliderInput(Vector2 v){
		if (v.x == 0){
			leverInput = v.y;
			if (leverInput > 0){
				audio.PlayOneShot (downSound);
			}
		}
	}
	
	void SpawnToast(){
		foreach (GameObject g in toastSpawners){
			totalToastsCreated += 2;
			GameObject newtoast = (GameObject)Instantiate (toast);
			newtoast.transform.parent = shipTransform;
			newtoast.transform.rotation = g.transform.rotation;
			newtoast.transform.position = g.transform.position;
			newtoast.rigidbody.velocity = g.transform.up * toastBoost;
			newtoast.GetComponent<Bread>().ignoreCol = toaster.collider;
			newtoast.GetComponent<Bread>().ignoring = true;

			Physics.IgnoreCollision(newtoast.collider, toaster.collider);
		}
	}

	void Update () {
		base.Update();
		if (currentPower > .08f){
			if (leverInput > 0){
				cookCount += Time.deltaTime;
				if (cookCount > toastCookTime){
					leverScript.ResetPos();
					cookCount = 0;
					SpawnToast();
					audio.PlayOneShot (popSound);
					audio.PlayOneShot (dingSound);

				}
			}
		}
		else{

		}
	}
}
