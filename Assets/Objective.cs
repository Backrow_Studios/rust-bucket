﻿using UnityEngine;
using System.Collections;

public class Objective : MonoBehaviour {
	[System.NonSerialized]
	public string objName = "";
	public bool filled = false;
	GameObject checkMark;

	void Start () {
		foreach(Transform c in transform){
			if (c.name == "Check Mark")
				checkMark = c.gameObject;
		}
		if (!filled)	
			checkMark.renderer.enabled = false;

		objName = name;
	}

	public void Check () {
		if (!filled){
			checkMark.renderer.enabled = true;
			filled = true;
		}
	}
}
