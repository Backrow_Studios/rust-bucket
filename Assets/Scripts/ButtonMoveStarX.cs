﻿using UnityEngine;
using System.Collections;

public class ButtonMoveStarX : MonoBehaviour {

	public GameObject StarController;

	public float Move;
	

	bool wasactive = false;
	void Update () {

		if (GetComponent<ButtonScript>().GetState () != wasactive){
			if (!GetComponent<ButtonScript>().GetState ()){
				StarController.GetComponent<SpaceStarSpawner>().directionSpeed.x = 0;
			}
			else{
				StarController.GetComponent<SpaceStarSpawner>().directionSpeed.x = Move;

			}
		}

		if ((StarController.GetComponent<SpaceStarSpawner>().directionSpeed.x <= 0 && Move > 0) || (StarController.GetComponent<SpaceStarSpawner>().directionSpeed.x >= 0 && Move < 0) || (Move == 0 && StarController.GetComponent<SpaceStarSpawner>().directionSpeed.x != 0)){
			if (GetComponent<ButtonScript>().GetState ()){
				GetComponent<ButtonScript>().TurnOff ();
			}
		}


		wasactive = GetComponent<ButtonScript>().GetState ();

	}
}
