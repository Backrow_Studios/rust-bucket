﻿using UnityEngine;
using System.Collections;

public class LandPanel : Panel {

	public AudioClip alarmSound;
	public AudioClip crashSound;

	public GameObject blinker;

	public GameObject lightObj;
	Light blinkLight;
	public GameObject leverHolder;

	public GameObject lever;
	SlideLever slideLever;

	Land landScript;
	public float driftPower = .2f;
	public float failAngle;

	public float failRumble = .5f;
	public float shakeDecay = .05f;

	MoveCamera movecamera;

	float failShake = 0;
	public bool failed = false;

	float localYStart = 0f;
	public float raiseSpeed = .1f;

	public AnimationCurve landSpeedCurve;

	void Start(){
		base.Start ();
		movecamera = Camera.main.GetComponent<MoveCamera>();
		failShake = failRumble;

		blinker.renderer.material.color = blinkOff;
		blink = false;

		blinkLight = lightObj.GetComponent<Light>();
		slideLever = lever.GetComponent<SlideLever>();
		localYStart = leverHolder.transform.localPosition.z;
	}
	
	void OnLevelWasLoaded(){
		if ( GameObject.Find ("Scenary")){
			landScript = GameObject.Find ("Scenary").GetComponent<Land>();
			landScript.failed = false;
			landScript.speedCurve = landSpeedCurve;
		}
		failed = false;

		failShake = failRumble;
	}
	
	float input = 0;
	
	public override void SliderInput(Vector2 v){
		if (v.x == 0){
			input = v.y;
		}
	}

	float blinkcount = 0f;
	public float blinkFreq = 1f;
	public Color blinkOn;
	public Color blinkOff;
	bool blink = false;
	void DoBlinking(){
		blinkcount += Time.deltaTime;
		if (blinkcount > blinkFreq){
			blinkcount = 0;
			if (!blink){
				blinker.renderer.material.color = blinkOn;
				blink = true;
				blinkLight.enabled = true;

			}
			else{
				blinker.renderer.material.color = blinkOff;
				blink = false;
				blinkLight.enabled = false;

			}
			
		}
		
	}

	void RaiseLever(){
		Vector3 v = leverHolder.transform.localPosition;
		if (v.z > 0){
			v.z -= raiseSpeed * Time.deltaTime * 60;
		}
		else if (v.y < 0){
			v.z = 0;
		}
		leverHolder.transform.localPosition = v;
		if (!slideLever.enabled){
			slideLever.enabled = true;
		}
	}
	
	void LowerLever(){
		Vector3 v = leverHolder.transform.localPosition;
		if (v.z < localYStart){
			v.z += raiseSpeed * Time.deltaTime * 60;
		}
		else if (v.y > localYStart){
			v.z = localYStart;
		}
		leverHolder.transform.localPosition = v;
		if (slideLever.enabled){
			slideLever.ResetImmediate();
			slideLever.enabled = false;
		}
	}
	bool crashed = false;
	void Update () {
		base.Update();
		bool raise = false;
		if (currentPower > .08f){
			if (landScript){

				if (!landScript.landed || Input.GetKey (KeyCode.B)){
					DoBlinking ();
					raise = true;
				}
				else{
					if (blink){
						blinker.renderer.material.color = blinkOff;
						blink = false;
						blinkLight.enabled = false;
					}
				}

				if (!failed){
					if (landScript.getAngle() > failAngle){
						failed = true;
						audio.PlayOneShot (alarmSound);
					}
					else if (landScript.getAngle() < -failAngle){
						failed = true;
						audio.PlayOneShot (alarmSound);
					}
				}

				if (!failed){

					if (landScript.getAngle() > 0){
						landScript.TurnRight (driftPower,true);
					}
					if (landScript.getAngle() <= 0){
						landScript.TurnRight (-driftPower,true);
					}
					landScript.TurnRight(input,false);
				}
				else{
					landScript.failed = true;
					if (landScript.landed){
						if (failShake > 0){
							failShake -= shakeDecay;
						}
						else{
							failShake = 0;
						}
					}
					movecamera.ShakeAmount = failShake;


				}

				if (!crashed){
					if (failed){
						if (landScript.landed){
							audio.PlayOneShot (crashSound);
							crashed = true;
						}
					}
				}
			}
			else{
				crashed = false;
			}

			if (raise){
				RaiseLever();
			}
			else{
				LowerLever();
			}
		}
		
	}
}
