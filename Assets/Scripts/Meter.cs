﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Meter : MonoBehaviour {


	LineRenderer myLine;

	
	public GameObject[] linepoints;
	List<Transform> points;
	public float bezel = 1;


	void Start () {
		points = new List<Transform>();
		foreach(GameObject g in linepoints){
			points.Add (g.transform);
		}

		myLine = GetComponent<LineRenderer>();
	}

	float fillLevel = 0f;
	public float FillLevel{
		get {
			return fillLevel;
		}
		set {
			fillLevel = value;
		}
	}

	void Update () {
		if (points.Count > 0){
			myLine.SetPosition(0,points[0].position);

			Vector3 dist = (points[1].position - points[0].position);
			myLine.SetPosition(1,points[0].position + (dist * fillLevel));
		}
	}
}
