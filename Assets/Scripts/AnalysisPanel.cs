﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnalysisPanel : Panel {

	public AudioClip dataSound;

	public GameObject Pic;
	public GameObject BootPic;

	public GameObject nameTextObj;
	public GameObject descriptionTextObj;

	TextMesh nameDisplay;
	TextMesh descriptionDisplay;

	public float rotateSpeed;

	void Start(){
		base.Start ();
		Pic.renderer.material = null;
		Pic.renderer.enabled = false;
		BootPic.renderer.enabled = false;

		nameTextObj.renderer.enabled = false;
		descriptionTextObj.renderer.enabled = false;

		nameDisplay = nameTextObj.GetComponent<TextMesh>();
		descriptionDisplay = descriptionTextObj.GetComponent<TextMesh>();

		nameDisplay.text = "ANALY-SYS";
		descriptionDisplay.text = "REVISION 14 HOLO";
	}
	
	void OnLevelWasLoaded(){
		if (Application.loadedLevelName == "buttons"){

		}
	}

	[System.NonSerialized]
	public bool used = false;

	public void NewData(Material tex, string name ,string text){
		if (currentPower > .093f) {
			audio.PlayOneShot (dataSound);
			Pic.renderer.material = tex;
			nameDisplay.text = WrapString (name);
			descriptionDisplay.text = WrapString (text);
			Pic.renderer.enabled = true;
			used = true;
		}
	}
	
	public int maxCharOnLine = 20;

	string WrapString(string s){

		int wrapcount = 0;

		for (int i = 0; i < s.Length; i++){
			wrapcount += 1;

			if (s[i] == '\n'){
				wrapcount = 0;
			}

			if (wrapcount > maxCharOnLine){
				wrapcount = 0;
				if (i > 0){
					if (s[i-1] != ' '){
						s = s.Insert (i,"\n-");

					}
					else{
						s = s.Insert (i,"\n");
					}
				}
			}

		}
		return s;
	}
	
	public override void ClickOn(int v){
		if (v == 0){
		}
		
	}
	public override void ClickOff(int v){
		if (v == 0){
		}

	}
	
	
	void Update () {
		base.Update ();
		if (currentPower > .093f){
			Pic.transform.localEulerAngles += new Vector3(0,rotateSpeed,0) * Time.deltaTime * 60;

			if (!Pic.renderer.enabled){
				//Pic.renderer.enabled = true;
				BootPic.transform.eulerAngles += new Vector3(0,rotateSpeed,0) * Time.deltaTime * 60;

				if (!BootPic.renderer.enabled){
					BootPic.renderer.enabled = true;
					nameTextObj.renderer.enabled = true;
					descriptionTextObj.renderer.enabled = true;		
				}
			}
			else{
				if (BootPic.renderer.enabled){
					BootPic.renderer.enabled = false;
				}
			}
		}
		else{
			if (Pic.renderer.enabled){
				BootPic.renderer.enabled = false;
				Pic.renderer.enabled = false;
				nameTextObj.renderer.enabled = false;
				descriptionTextObj.renderer.enabled = false;

			}
		}
	}
}
