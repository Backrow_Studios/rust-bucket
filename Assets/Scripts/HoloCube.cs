﻿using UnityEngine;
using System.Collections;

public class HoloCube : MonoBehaviour {
	public GameObject Body;
	Material myTexture;

	public AudioClip GrabSound;
	public AudioClip HitSound;


	public float followtween;
	public float floatAboveAmount = .1f;

	public Vector3 heldBodyPos;
	public float scaleTween;
	public float rotateTween;
	Vector3 expandedSize;
	public Vector3 shrinkSize;

	public float flickerAlpha;
	float flickercount;
	public float flickerFreq = 10;
	float startAlpha;

	GameObject holoLight;

	LineRenderer mylinerenderer;

	public AnalysisPanel analysisPanel;

	public string name = "???";
	public string description = "ERROR: UNIDENTIFIED OBJECT IN SCANNER";

	[System.NonSerialized]
	public bool readyToDelete = false;

	void Start () {
		mylinerenderer = GetComponent<LineRenderer>();

		holoLight = GameObject.Find ("HoloLight");
		Physics.IgnoreLayerCollision(9,0);
		Physics.IgnoreLayerCollision(9,1);
		//Physics.IgnoreLayerCollision(9,2);
		expandedSize = Body.transform.localScale;

		startAlpha = Body.renderer.material.color.a;
		dimmermult = 0;
		//audio.PlayOneShot(GrabSound);

		analysisPanel = GameObject.Find ("AnalysisPanel").GetComponent<AnalysisPanel>();

		myTexture = null;
		foreach (Transform c in Body.transform){
			myTexture = c.renderer.material;
		}
	}

	bool f = false;
	void Doflicker(){
		Color c = Body.renderer.material.color;

		flickercount += 1;
		if (flickercount > flickerFreq){
			flickercount = 0;
			if (f){
				c.a = flickerAlpha;
				c.a *= dimmermult;

				f = false;
			}
			else{
				c.a = startAlpha;
				c.a *= dimmermult;

				f = true;
			}
		}
		Body.renderer.material.color = c ;
	}

	public float collisionDimmerThreshold = 1f;
	float dimmermult = 1f;
	public float dimAmount = .2f;
	public float dimTween = .1f;


	void OnCollisionEnter(Collision col){
		if(!rigidbody.isKinematic){
			if (rigidbody.velocity.magnitude > collisionDimmerThreshold){
				dimmermult = rigidbody.velocity.magnitude * dimAmount;
				audio.PlayOneShot(HitSound);
			}
			if (col.gameObject.name == "HoloEnd"){
				if (readyToDelete){
					analysisPanel.NewData(myTexture,name,description);
					Destroy (gameObject);
				}
			}

		}

	}

	void Release(Vector3 force){
		held = false;
		rigidbody.useGravity = true;
		rigidbody.freezeRotation = false;
		rigidbody.isKinematic = false;
		
		rigidbody.velocity = force;
	}


	public Color startLineColor;
	public Color EndLineColor;

	void Update () {
		dimmermult += ((1 - dimmermult) * dimTween) * Time.deltaTime * 60;
		Doflicker();

		mylinerenderer.SetPosition(0,holoLight.transform.position);
		mylinerenderer.SetPosition(1,Body.transform.position);
		Color r = startLineColor;
		r.a = Random.Range(.15f,.27f);
		mylinerenderer.SetColors (r,EndLineColor);

		                                        
		if (held){
			mylinerenderer.enabled = true;

			rigidbody.useGravity = false;
			rigidbody.isKinematic = true;
			rigidbody.freezeRotation = true;

			transform.position = cursor.transform.position; //+ dif;
			transform.position -= (Camera.main.transform.position-cursor.transform.position) * floatAboveAmount;

			Quaternion targetrot = cursor.transform.rotation;

			float angle = Quaternion.Angle(transform.rotation,targetrot);
			float step = angle * rotateTween;

			transform.rotation = Quaternion.RotateTowards(transform.rotation,targetrot,step * Time.deltaTime * 60);


			Vector3 d = (expandedSize - Body.transform.localScale);
			Vector3 c = d * scaleTween;
			Body.transform.localScale += c * Time.deltaTime * 60;
			//c.x = 0;
			//Body.transform.localPosition += (c / 2) * Time.deltaTime * 60;
			Body.transform.localPosition += ((heldBodyPos - Body.transform.localPosition)*scaleTween) * Time.deltaTime * 60;

		}
		else{
			mylinerenderer.enabled = false;

			Vector3 d = (shrinkSize - Body.transform.localScale);
			Vector3 c = d * scaleTween;
			Body.transform.localScale += c * Time.deltaTime * 60;
			Body.transform.localPosition += ((Vector3.zero - Body.transform.localPosition)*scaleTween) * Time.deltaTime * 60;
		}
		if (Input.GetButtonUp("Fire1") && held){
			Release (Vector3.zero);
		}
		
	}

	Vector3 dif;
	bool held = false;
	GameObject cursor;
	void Touched(GameObject g){
		cursor = g;
		held = true;
		audio.PlayOneShot(GrabSound);

		//dif = transform.position - cursor.transform.position;
		//dif.z = 0;
	}
}
