﻿using UnityEngine;
using System.Collections;

public class TextAway : MonoBehaviour {

	public float stayTime;
	float stayCount = 0f;

	TextMesh text;
	void Start(){
		text = GetComponent<TextMesh>();
	}

	// Update is called once per frame
	void Update () {
		stayCount += Time.deltaTime;
		if (stayCount > stayTime){
			text.text = "";
			this.enabled = false;
		}
	}
}
