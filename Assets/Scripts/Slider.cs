﻿using UnityEngine;
using System.Collections;

public class Slider : MonoBehaviour {

	public float slideRange;
	public string slideAxis;

	public float startValue;


	public float MoveTween;

	public float MaxStep;

	public GameObject PANEL;
	public int SendVal;

	float VALUE;

	bool held = false;

	Vector3 startpos;

	float MaxX;
	float MinX;
	float MaxY;
	float MinY;

	GameObject cursorpoint;
	MouseFollow MF;
	void Start () {
		startpos = transform.localPosition;
		MaxX = startpos.x + slideRange;
//		MinX = startpos.x - slideRange;
		MaxY = startpos.y + slideRange;
		MinY = startpos.y - slideRange;

		ResetPos();
	}

	public void ResetPos(){
		if (slideAxis == "x"){
			float range = MaxX - MinX;
			float newx = MinX + startValue * range;
			transform.localPosition = new Vector3(newx,transform.localPosition.y,transform.localPosition.z);
		}
		if (slideAxis == "y"){
			float range = MaxY - MinY;
			float newy = MinY + startValue * range;
			transform.localPosition = new Vector3(transform.localPosition.x,newy,transform.localPosition.z);
		}
	}

	GameObject cursor;
	void Touched(GameObject g){
		cursor = g;
		held = true;
		MF = cursor.GetComponent<MouseFollow>();
		cursorpoint = MF.GetComponent<MouseFollow>().MyPoint;
	}

	public float GetValue(){
		return VALUE;
	}
	
	void Update () {
		float prevVal = VALUE;
		if (held){
			//Vector3 mouse =	cursor.GetComponent<MouseFollow>().lineendpoint;
			//Vector3 target = new Vector3(mouse.x,mouse.y,10);
			Vector3 destination = MF.lineendpoint + (transform.position - cursor.transform.position);//Camera.main.ScreenToWorldPoint(target) - cursor.transform.localPosition;
		
			if (slideAxis == "x"){
				float d = destination.x - transform.position.x;
				float c = d * MoveTween;

				if (c > MaxStep){
					c = MaxStep;
				}
				if (c < -MaxStep){
					c = -MaxStep;
				}


				transform.localPosition += new Vector3(c * Time.deltaTime * 60,0,0);

				if (transform.localPosition.x > MaxX){
					transform.localPosition = new Vector3(MaxX, transform.localPosition.y,transform.localPosition.z);
				}
				if (transform.localPosition.x < MinX){
					transform.localPosition = new Vector3(MinX, transform.localPosition.y,transform.localPosition.z);
				}

			}
			if (slideAxis == "y"){
				float d = destination.y - transform.position.y;
				float c = d * MoveTween;
				
				if (c > MaxStep){
					c = MaxStep;
				}
				if (c < -MaxStep){
					c = -MaxStep;
				}
				
				
				transform.localPosition += new Vector3(0,c * Time.deltaTime * 60,0);
				
				if (transform.localPosition.y > MaxY){
					transform.localPosition = new Vector3(transform.localPosition.x, MaxY,transform.localPosition.z);
				}
				if (transform.localPosition.y < MinY){
					transform.localPosition = new Vector3(transform.localPosition.x, MinY,transform.localPosition.z);
				}
				
			}
			
			if (Input.GetButtonUp("Fire1")){
				held = false;
			}
		}

		if (slideAxis == "x"){
			float range = MaxX - MinX;
			float val = transform.localPosition.x - MinX;

			VALUE = val/range;
		}
		if (slideAxis == "y"){
			float range = MaxY - MinY;
			float val = transform.localPosition.y - MinY;
			
			VALUE = val/range;
		}

		if (PANEL){
			if (prevVal != VALUE){
				PANEL.SendMessage ("SliderInput", new Vector2(SendVal,VALUE),SendMessageOptions.DontRequireReceiver);
			}
		}

	}
}
