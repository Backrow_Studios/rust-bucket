﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class DavePanel : Panel {
	
	public List<GameObject> graphs;
	List<LineGraph> graphList;
	public float graphTween;

	List<float> graphAmpList = new List<float>();

	void Start(){
		base.Start();
		graphList = new List<LineGraph>();
		foreach (GameObject g in graphs){
			graphList.Add (g.GetComponent<LineGraph>());
			graphAmpList.Add (0);
		}
	}

	public override void ClickOn(int v){

	}
	
	public override void ClickOff(int v){

	}	

	public override void SliderInput(Vector2 v){
		graphAmpList[(int)v.x] = v.y;
	}

	void Update(){
		base.Update();
		if (currentPower > 0){
			for (int i = 0; i < graphList.Count; i++){
				graphList[i].wavelength += ((2 - graphList[i].wavelength) * graphTween) * Time.deltaTime * 60;
				graphList[i].amplitude += ((graphAmpList[i] - graphList[i].amplitude) * graphTween) * Time.deltaTime * 60;
				graphList[i].scrollspeed += (( (graphAmpList[i] * .08f) - graphList[i].scrollspeed) * graphTween) * Time.deltaTime * 60;
			}
		}
		else{
			for (int i = 0; i < graphList.Count; i++){
				graphList[i].wavelength += ((1 - graphList[i].wavelength) * graphTween) * Time.deltaTime * 60;
				graphList[i].amplitude += ((0 - graphList[i].amplitude) * graphTween) * Time.deltaTime * 60;
				graphList[i].scrollspeed += ((0 - graphList[i].scrollspeed) * graphTween) * Time.deltaTime * 60;
			}
		}
	}

}
