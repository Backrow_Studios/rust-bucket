﻿using UnityEngine;
using System.Collections;

public class SlideLever : MonoBehaviour {
	
	public GameObject rotatePoint;
	public GameObject switchBody;

	public GameObject BreakablePart;
	public float breakthreshold;
	float breakcount;

	public float rotateRange;
	
	public float inBetweenTurnMult = 1f;
	
	public string slideAxis = "x";
	public float flickThreshold;
	
	bool offon = false;
	
	public float startValue;
	
	
	public float MoveTween;
	public float MoveSpeed;

	public float returnToCenterTween = 0f;

	public GameObject PANEL;
	public int SendVal;
	
	public AudioClip flickOnSound;
	public AudioClip flickOffSound;
	
	float VALUE;
	
	bool held = false;
	
	Vector3 startpos;
	
	float MaxX;
	float MinX;
	float MaxY;
	float MinY;
	
	GameObject cursorpoint;
	MouseFollow MF;

	BreakEquipment breaker;

	Quaternion startRot;
	void Start () {
		startRot = rotatePoint.transform.rotation;
		switchBody.transform.parent = rotatePoint.transform;
		turn = startValue;
		if(BreakablePart){
			breaker = gameObject.GetComponent<BreakEquipment>();
		}
	}
	
	
	GameObject cursor;
	void Touched(GameObject g){
		cursor = g;
		held = true;
		MF = cursor.GetComponent<MouseFollow>();
		cursorpoint = MF.GetComponent<MouseFollow>().MyPoint;
	}
	
	public float GetValue(){
		return VALUE;
	}
	
	public void ResetPos(){

		turn = startValue;
		
	}

	public void ResetImmediate(){
		
		rotatePoint.transform.rotation = startRot;
	}


	float turn = 0;
	float angle = 0;
	float prevVal = 0;
	void Update () {
		if (held){
			Vector3 destination = MF.lineendpoint + (transform.position - cursor.transform.position);//Camera.main.ScreenToWorldPoint(target) - cursor.transform.localPosition;
			Vector3 dest = MF.lineendpoint - MF.MyPoint.transform.position;
			float d = 0;
			if (slideAxis == "x"){
				//d = destination.x - transform.position.x;
				if (Mathf.Abs (dest.x)> Mathf.Abs (dest.z))
					d = dest.x;
				else
					d = dest.z;
			}
			if (slideAxis == "y"){
				//d = destination.y - transform.position.y;
				d = -dest.y;
			}

			turn += (-d * MoveSpeed);



			
			if (Input.GetButtonUp("Fire1")){
				held = false;
			}
		}

		turn += ((0-turn)* returnToCenterTween);

		if (turn > rotateRange){
			turn = rotateRange;
		}
		else if (turn < -rotateRange){
			turn = -rotateRange;
		}

		angle += ((turn - angle)*MoveTween) * Time.deltaTime *60;

		rotatePoint.transform.localEulerAngles = new Vector3(angle,0,0);

		VALUE = (turn) / (rotateRange);

		if (BreakablePart){
			breakcount = VALUE;
			if (breakcount > breakthreshold){
				breaker.BreakOff(BreakablePart);
				held = false;
				this.enabled = false;
			}
		}

		if (PANEL){
			if (prevVal != VALUE){
				if (Mathf.Abs(VALUE) > .001f){
					PANEL.SendMessage ("SliderInput", new Vector2(SendVal,VALUE),SendMessageOptions.DontRequireReceiver);
				}
				else{
					PANEL.SendMessage ("SliderInput", new Vector2(SendVal,0),SendMessageOptions.DontRequireReceiver);
				}
				prevVal = VALUE;

			}
		}

		
	}
}
