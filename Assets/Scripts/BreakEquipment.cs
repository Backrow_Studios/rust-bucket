﻿using UnityEngine;
using System.Collections;

public class BreakEquipment : MonoBehaviour {

	Transform shipTransform;

	MouseFollow cursor;

	public AudioClip breakSound;

	void Start () {
		cursor = GameObject.Find ("Cursor").GetComponent<MouseFollow>();

		shipTransform = GameObject.Find ("SHIP").transform;
	}

	public void BreakOff(GameObject b){
		if(breakSound)
			audio.PlayOneShot (breakSound);


		b.transform.parent = shipTransform;
		if (!b.rigidbody){
			b.AddComponent("Rigidbody");
		}
		b.AddComponent("BrokenThing");

		b.name = "brokenthing";
		b.tag = "physical";
		b.layer = 9;
		b.rigidbody.isKinematic = false;
		b.rigidbody.useGravity = true;
		b.rigidbody.constraints = RigidbodyConstraints.None;
		b.rigidbody.velocity = (cursor.lineendpoint - cursor.MyPoint.transform.position) * cursor.throwForce;
		cursor.LetGo();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
