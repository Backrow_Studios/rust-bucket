﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NavPanel : Panel {

	public GameObject movePanel;

	public AudioClip errorSound;

	public List<GameObject> planetList;
	public List<Quaternion> planetRotationList;

	MoveCamera movecamera;

	public GameObject PlanetHolder;

	public GameObject StarController;
	int curplanet = 0;

	public GameObject Pic;

	bool targeted = false;

	MovePanel movepanel;

	void Start(){
		base.Start ();
		movepanel = movePanel.GetComponent<MovePanel>();
		movecamera = Camera.main.GetComponent<MoveCamera>();
		//planetList = new List<GameObject>();
	}

	void OnLevelWasLoaded(){
		if (Application.loadedLevelName == "buttons"){
			PlanetHolder = GameObject.Find ("PLANET HOLDER");
			StarController = PlanetHolder;
			planetList = new List<GameObject>();
			planetRotationList = new List<Quaternion> ();


			foreach (Transform c in PlanetHolder.transform){
				if (c.name == "PLANET1" || c.name == "PLANET2" || c.name == "PLANET3" || c.name == "PLANET4" || c.name == "PLANET5")
					planetList.Add (c.gameObject);
			}


			if(planetList.Count > 0)	{
				for (int i = 0; i < planetList.Count; i++){

					Vector3 p = planetList[i].transform.position;
					p.x *= -1;
					
					Vector3 v = Camera.main.transform.position - p;
					Quaternion q = Quaternion.LookRotation(-v);
					planetRotationList.Add (q);
				}
			}

			ChangedSelection();
		}
	}

	public GameObject SelectedPlanet{
		get{
			return planetList[curplanet];
		}
	}

	public bool Targeted{
		get{
			return targeted;
		}
	}

	void ChangedSelection(){
		active = true;
		targeted = false;
		if (planetList.Count > 0){
			if (planetList[curplanet]){
				Pic.renderer.material.mainTexture = planetList[curplanet].GetComponent<PlanetaryBody>().mapGraphic;
			}
			else{
				Pic.renderer.material.mainTexture = null;
				audio.PlayOneShot(errorSound);
			}
		}
	}

	bool active = false;
	public override void ClickOn(int v){
		if (v == 0){
			//roll.y = -1;
		}
		if (v == 2){
			//active = true;
		}
		if (v == 1){
			//roll.x = -1;

		}
		
	}
	public override void ClickOff(int v){
		if (v == 0){
			SelLeft();
			active = true;

		}
		if (v == 2){
			//active = false;
		}
		if (v == 1){
			SelRight();
			active = true;

		}
	}

	void SelRight(){
		if(Time.time > 0)	{

			if (!movepanel.Moving){
				curplanet += 1;
				if (curplanet >= planetList.Count){
					curplanet = 0;
				}
				ChangedSelection();
			}
			else{
				audio.PlayOneShot(errorSound);
			}
		}
	}

	void SelLeft(){
		if(Time.time > 0)	{
			if (!movepanel.Moving){
				curplanet -= 1;
				if (curplanet < 0){
					curplanet = planetList.Count - 1;
				}
				ChangedSelection();
			}
			else{
				audio.PlayOneShot(errorSound);
			}
		}		

	}

	void RotateTowardsPlanet(){
		if (PlanetHolder){
			Vector3 v = Camera.main.transform.position - planetList[curplanet].transform.position;

			Quaternion q = Quaternion.LookRotation(-v,Vector3.up);
			//Q is the way we want to point

			Vector3 c = Vector3.forward;
			Vector3 d = q.eulerAngles;

			//q = Quaternion.Euler (StarController.transform.rotation.eulerAngles + (c - d));
			q = Quaternion.Inverse(q) * Quaternion.Euler (c) * StarController.transform.rotation;


			movecamera.RotateShipTowards(q);
			targeted = true;
		}
		else{
			active = false;
		}

	}



	void Update () {
		base.Update ();
		if (currentPower > .093f){
			if (active){
				RotateTowardsPlanet();
			}
			if (!Pic.renderer.enabled){
				Pic.renderer.enabled = true;
				ChangedSelection();
			}
		}
		else{
			if (Pic.renderer.enabled){
				Pic.renderer.enabled = false;
			}
		}
	}
}
