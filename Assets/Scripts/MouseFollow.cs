﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MouseFollow : MonoBehaviour {


	[System.NonSerialized]
	public GameObject GrabbedObject;

	public Object holoGram;
	public float hologramSize;

	public GameObject MyPoint;
	public GameObject MyRing;
	SpriteRenderer ringRenderer;

	public float moveTween = .5f;

	RaycastHit[] hits;
	bool raycastthisframe = false;
	float CursorZ;
	public float CursorZTween;
	public float CursorDistanceVar;

	public float LeanMultiplier = 10f;

	public Vector3 smallSize;
	public float sizeTween;
	Vector3 startRingSize;

	
	public float mouseSpeed;
	public float stretchMult;
	public float maxStretch;

	public float throwForce = 1f;

	[System.NonSerialized]
	public Vector3 virtualPointer = Vector3.zero;

	float curmovetween;

	LineRenderer linerenderer;

	Transform startParent;

	List<string> interactList = new List<string>();

	Color myColor;

	[System.NonSerialized]
	public int totalButtonPresses = 0;

	void Start () {
		startParent = transform.parent;
		curmovetween = moveTween;
		Screen.showCursor = false;
		startRingSize = MyRing.transform.localScale;
		//virtualPointer += new Vector3(Screen.width /2,Screen.height /2,0);
		virtualPointer = Camera.main.WorldToScreenPoint (transform.position);
		linerenderer = GetComponent<LineRenderer>();

		MyPoint.renderer.sortingLayerName = "LayerName";
		MyPoint.renderer.sortingOrder = 2;
		linerenderer.sortingLayerName = "LayerName";
		linerenderer.sortingOrder = 2;

		myColor = MyPoint.renderer.material.color;
		ringRenderer = MyRing.GetComponent<SpriteRenderer>();
		SetupInteract();
	}

	void SetupInteract(){
		interactList.Add("button");
		interactList.Add("pianobutton");
		interactList.Add("slider");
		interactList.Add("flickswitch");
		interactList.Add("knob");
		interactList.Add("pullcord");
		interactList.Add("slidelever");

 		interactList.Add("physical");
	}

	Vector3 hitp = Vector3.zero;
	public Vector3 GetHitPoint(){
		DoRaycast();
		return hitp;
	}

	//dotint : Do a tint this frame?
	bool dotint = false;
	bool tinted = false;
	void TintOn(){
		if (!tinted){
			tinted = true;
			Color c = (myColor + Color.white) /2;
			MyPoint.renderer.material.color = c;
			ringRenderer.color = c;
			linerenderer.material.color = c;
		}
	}
	void TintOff(){
		if (tinted){
			tinted = false;
			Color c = myColor;
			MyPoint.renderer.material.color = c;
			ringRenderer.color = c;
			linerenderer.material.color = c;
		}
	}

	void DoRaycast(){
		if (!raycastthisframe){
			raycastthisframe = true;
			Ray ray = Camera.main.ScreenPointToRay(virtualPointer);

			hits = Physics.RaycastAll(ray);

			float closesthit = 1000;
			foreach (RaycastHit hit in hits){


				if (interactList.Contains (hit.transform.gameObject.tag)){
					dotint = true;
				}

				if (hit.transform.gameObject.tag == "cursorhover"){
					float distfromcamera = Vector3.Distance(hit.point,Camera.main.transform.position);
					if (distfromcamera< closesthit){

						hitp = hit.point;
						closesthit = distfromcamera;
					}
				}
			}
		}
	}

	
	//returns mouse movement relative to rotation
	Vector3 GetRelativeMouseMovement(){
		Vector3 move;
		Vector3 horizontal = Camera.main.transform.right * Input.GetAxis ("Mouse X");
		Vector3 vertical = Camera.main.transform.up * Input.GetAxis ("Mouse Y");
		move = horizontal + vertical;
		move *= mouseSpeed;
		return move;
	}


	bool bigring = false;
	void DoRing(){
		if (bigring) {
			Vector3 target = startRingSize;
			Vector3 d = target - MyRing.transform.localScale;
			Vector3 c = d * sizeTween;

			MyRing.transform.localScale += c * Time.deltaTime * 60;
		} 
		else {
			Vector3 target = smallSize;
			Vector3 d = target - MyRing.transform.localScale;
			Vector3 c = d * sizeTween;
			
			MyRing.transform.localScale += c * Time.deltaTime * 60;
		}
	}

	void FaceCamera(){
		Vector3 v = Camera.main.transform.position - transform.position;
		transform.rotation = Quaternion.LookRotation(-v,Camera.main.transform.up) * Quaternion.Euler(0, 0, 0);
	}
	Vector3 returnvec = Vector3.zero;
	Vector3 endpointvec = Vector3.zero;

	[System.NonSerialized]
	public Vector3 lineendpoint = Vector3.zero;
	void FacePointer(){
		DoRaycast();

		Vector3 zt = Vector3.zero;
		Vector3 zz = Vector3.zero;

		Ray ray = Camera.main.ScreenPointToRay(Camera.main.WorldToScreenPoint(transform.position));
		RaycastHit[] hitz = Physics.RaycastAll(ray);
		float closesthit = 1000;

		foreach (RaycastHit hit in hitz){
			if (hit.transform.gameObject.tag == "cursorhover"){
				float distfromcamera = Vector3.Distance(hit.point,Camera.main.transform.position);

				if (distfromcamera < closesthit){
					zz = hit.point;
					closesthit = distfromcamera;
				}
			}
		}
		closesthit = 1000;
		foreach (RaycastHit hit in hits){
			if (hit.transform.gameObject.tag == "cursorhover"){
				float distfromcamera = Vector3.Distance(hit.point,Camera.main.transform.position);

				if (distfromcamera < closesthit){
					zt = hit.point;
					closesthit = distfromcamera;

				}
			}
		}

		Vector3 Dif = zt - zz;


		//movementlastframe = Input.mousePosition - pos1;



		movementlastframe = new Vector3 (Input.GetAxis ("Mouse X"),Input.GetAxis ("Mouse Y"),0);

		if (Input.GetButton("Fire2") && !Input.GetButton("Fire1")){
			movementlastframe = Vector3.zero;
		}

		movementlastframe *= mouseSpeed;
		//pos1 = Input.mousePosition;



		linerenderer.SetPosition(0,MyPoint.transform.position);

		if (GrabbedObject){
			Vector3 move = GetRelativeMouseMovement();

			returnvec += ((Vector3.zero - returnvec) * .1f);
			returnvec +=(Quaternion.Euler(transform.eulerAngles) * movementlastframe) * .01f;

			endpointvec	+= ((Vector3.zero - endpointvec) * .1f);
			endpointvec += move * .01f;

			//Vector3 returntemp = Quaternion.Euler(transform.eulerAngles) * returnvec;
			Vector3 r = (endpointvec * stretchMult);

			if (noparent){
				virtualPointer += movementlastframe;
				r = endpointvec * 1;
			}

			if (r.magnitude > maxStretch){
				r = r.normalized * maxStretch;
			}



			lineendpoint = (MyPoint.transform.position + returnvec + heldtravel);

			linerenderer.SetPosition(1,(MyPoint.transform.position + r + heldtravel));
		}
		else{
			virtualPointer += movementlastframe;


			returnvec = Vector3.zero;
			endpointvec = Vector3.zero;
			lineendpoint = (MyPoint.transform.position + Dif + heldtravel);

			linerenderer.SetPosition(1,(MyPoint.transform.position + Dif + heldtravel));
		}
		//pos1 = transform.position;

	}

	Vector3 BindVec;
	public void MovePointer(Vector3 m){
		virtualPointer += m;

	}

	void ResetPointer(){
		MyPoint.transform.localRotation = Quaternion.Euler(0,0, 0);
	}


	void LateUpdate(){
		linerenderer.SetPosition(0,MyPoint.transform.position);
		if (Input.GetButton("Fire2") && !Input.GetButton("Fire1")){
			linerenderer.SetPosition(1,MyPoint.transform.position);
		}

		if (dotint){
			TintOn();
		}
		else{
			TintOff ();
		}
	}

	//Vector3 LastHitPos;

	Vector3 heldtravel = Vector3.zero;
	Vector3 movementlastframe = Vector3.zero;
	//Vector3 pos1 = Vector3.zero;
	bool noparent = false;
	void Update () {
		dotint = false;
		raycastthisframe = false;

		if (Input.GetButtonUp("Fire1")){
			//Reset pointer position if using a control that moves around
			if (GrabbedObject){
				if(GrabbedObject.tag == "slider" || GrabbedObject.tag == "slidelever" || GrabbedObject.tag == "flickswitch" || GrabbedObject.tag == "physical"){
						virtualPointer = Camera.main.WorldToScreenPoint (transform.position);
					}
			}
		}


		if (Input.GetButton("Fire1") && (GrabbedObject) && !noparent){
			bigring = true;
			dotint = true;
		}
		else{ 
			heldtravel = Vector3.zero;
			bigring = false;

			if (GrabbedObject){
				dotint = true;
				bigring = true;
			}

			//Vector3 MousePos = Input.mousePosition;

			DoRaycast();
			Vector3 zt = Vector3.zero;
			float closesthit = 1000;
			foreach (RaycastHit hit in hits){
				if (hit.transform.gameObject.tag == "cursorhover" || hit.transform.gameObject.layer == 8){
					float distfromcamera = Vector3.Distance(hit.point,Camera.main.transform.position);

					if (distfromcamera < closesthit){
						zt = hit.point;
						
						//LastHitPos = zt;
						closesthit = distfromcamera;
					}
				}
			}
			Vector3 target = zt;
			target = target + ((target - Camera.main.transform.position) * CursorDistanceVar);
			Vector3 destination = target;
			Vector3 d = destination - transform.position;
			Vector3 c = d * curmovetween;

			transform.position += c * Time.deltaTime * 60;
		}

		if(Input.GetButtonDown("Fire1")){
			Screen.lockCursor = true;

			GrabSomething();
		}

		if (noparent == true){
			if (GrabbedObject)
				if (GrabbedObject.tag == "pianobutton"){
					LookPiano();
				}
		}


		if (Input.GetButtonUp("Fire1")){
			if (GrabbedObject){
				if(GrabbedObject.tag == "physical"){
					GrabbedObject.SendMessage("Release",(lineendpoint - MyPoint.transform.position) * throwForce,SendMessageOptions.DontRequireReceiver);
				}
			}

			LetGo();
		}
		FacePointer ();
		FaceCamera();
		DoRing ();

		//print (virtualPointer);
	}

	public void LetGo(){
		GrabbedObject = null;
		transform.parent = startParent;
		noparent = false;
		curmovetween = moveTween;
	}

	void LookPiano(){

		DoRaycast ();
		
		float closesthit = 1000;
		GameObject hit = null;
		foreach (RaycastHit hitted in hits){
			if (hitted.transform.gameObject.layer != 10){
				float distfromcamera = Vector3.Distance(hitted.point,Camera.main.transform.position);
				
				if (distfromcamera < closesthit){
					hit = hitted.transform.gameObject;
					
					closesthit = distfromcamera;
				}
			}
		}
		
		if (hit){
			if (hit.gameObject != GrabbedObject){
				if (hit.transform.gameObject.tag == "pianobutton"){
					GrabbedObject.SendMessage("Release",gameObject,SendMessageOptions.DontRequireReceiver);
					GrabbedObject = null;
					GrabbedObject = hit.transform.gameObject;
					GrabbedObject.SendMessage("Touched",gameObject,SendMessageOptions.DontRequireReceiver);
					
					noparent = true;
					//transform.parent = hit.transform;
				}
			}
		}
	}

	void GrabSomething(){
		
		DoRaycast ();
		
		float closesthit = Mathf.Infinity;
		GameObject hit = null;
		foreach (RaycastHit hitted in hits){
			if (hitted.transform.gameObject.layer != 10){
				float distfromcamera = Vector3.Distance(hitted.point,Camera.main.transform.position);

				if (distfromcamera < closesthit){
					hit = hitted.transform.gameObject;
					
					closesthit = distfromcamera;
				}
			}
		}
		
		if (hit){
			if (hit.transform.gameObject.tag == "button"){
				totalButtonPresses += 1;
				GrabbedObject = hit.transform.gameObject;
				GrabbedObject.SendMessage("Touched",gameObject,SendMessageOptions.DontRequireReceiver);
				
				transform.parent = hit.transform;
			}
			if (hit.transform.gameObject.tag == "pianobutton"){
				GrabbedObject = hit.transform.gameObject;
				GrabbedObject.SendMessage("Touched",gameObject,SendMessageOptions.DontRequireReceiver);

				noparent = true;
			}
			if (hit.transform.gameObject.tag == "slider"){
				GrabbedObject = hit.transform.gameObject;
				GrabbedObject.SendMessage("Touched",gameObject,SendMessageOptions.DontRequireReceiver);
				
				transform.parent = hit.transform;
			}
			if (hit.transform.gameObject.tag == "slidelever"){
				GrabbedObject = hit.transform.parent.parent.gameObject;
				GrabbedObject.SendMessage("Touched",gameObject,SendMessageOptions.DontRequireReceiver);
				
				transform.parent = hit.transform;
			}
			if (hit.transform.gameObject.tag == "flickswitch"){
				GrabbedObject = hit.transform.parent.parent.parent.gameObject;
				GrabbedObject.SendMessage("Touched",gameObject,SendMessageOptions.DontRequireReceiver);

				transform.parent = hit.transform;
			}
			if (hit.transform.gameObject.tag == "knob"){
				GrabbedObject = hit.transform.gameObject;
				GrabbedObject.SendMessage("Touched",gameObject,SendMessageOptions.DontRequireReceiver);
				
				transform.parent = hit.transform;
			}
			if (hit.transform.gameObject.tag == "pullcord"){
				GrabbedObject = hit.transform.parent.parent.gameObject;

				if (!GrabbedObject.GetComponent<Pullcord>()){
					GrabbedObject = hit.transform.parent.gameObject;
				}

				GrabbedObject.SendMessage("Touched",gameObject,SendMessageOptions.DontRequireReceiver);
				
				transform.parent = hit.transform;
			}
			
			if (hit.transform.gameObject.tag == "physical"){
				GrabbedObject = hit.transform.gameObject;
				GrabbedObject.SendMessage("Touched",gameObject,SendMessageOptions.DontRequireReceiver);
				
				noparent = true;	
				
				if (GrabbedObject.GetComponent<HoloCube>()){
					curmovetween = GrabbedObject.GetComponent<HoloCube>().followtween;
				}
				if (GrabbedObject.GetComponent<Bread>()){
					curmovetween = GrabbedObject.GetComponent<Bread>().followtween;
				}
				if (GrabbedObject.GetComponent<BrokenThing>()){
					curmovetween = GrabbedObject.GetComponent<BrokenThing>().followtween;
				}
			}
			
			if (hit.transform.gameObject.GetComponent<Hologramable>()){
				GrabbedObject = hit.transform.gameObject;

				if (GrabbedObject.GetComponent<Hologramable>().Body.renderer.enabled){

					//Get name and stuff
					string name = GrabbedObject.GetComponent<Hologramable>().name;
					string desc = GrabbedObject.GetComponent<Hologramable>().desc;

					//get coordinates
					//Rect grabrect = GrabbedObject.GetComponent<Hologramable>().GUIRectWithObject();
					
					//creat hologram
					GameObject NewHolo = (GameObject)Instantiate(holoGram);
					GameObject NewBody = (GameObject)Instantiate(GrabbedObject.GetComponent<Hologramable>().Body);
					Vector2 newsize = NewBody.transform.localScale;

					NewHolo.name = "HoloCube";
					newsize = newsize.normalized * hologramSize;
					NewBody.transform.localScale = newsize;
					NewBody.transform.parent = NewHolo.GetComponent<HoloCube>().Body.transform;
					NewBody.transform.localPosition = Vector3.zero;
					NewBody.transform.localEulerAngles += new Vector3(0,180,0);
					NewHolo.transform.rotation = transform.rotation;

					//alpha
					Color c = NewBody.renderer.material.color;
					c.a = .6f;
					NewBody.renderer.material.color = c;
					
					Vector3 n = transform.position;
					NewHolo.transform.position = n;


					//BAD
					NewHolo.transform.parent = GameObject.Find ("SHIP").transform;

					//grab it
					GrabbedObject = NewHolo;
					GrabbedObject.SendMessage("Touched",gameObject,SendMessageOptions.DontRequireReceiver);
					
					noparent = true;	

					HoloCube holocube = GrabbedObject.GetComponent<HoloCube>();

					if (holocube){
						holocube.name = name;
						holocube.description = desc;
						curmovetween = GrabbedObject.GetComponent<HoloCube>().followtween;
					}
				}
				else{
					GrabbedObject = null;
				}
			}
			
			
		}
	}
}
