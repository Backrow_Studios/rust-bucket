﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpaceStarSpawner : MonoBehaviour {
	public GameObject star;
	//var for x bounds ( -110 - 110)
	//public int leftBound = -110;
	//public int rightBound = 110;

	//var for z bounds (45 - 0)
	//public int farZBound = 45;
	//public int closeZBound = -9;

	//var for height bounds (-43 - 43)
	//public int lowerBound = -43;
	//public int upperBound = 43;

	public Vector2 outsideCameraRange;
	public float farSpawnPlane;


	public int howManyStars;

	public Vector3 directionSpeed;
	public bool turning = false;
	GameObject parent;

	public GameObject Cam;


	List<GameObject> spawnedList;
	// Use this for initialization
	void Spawn () {
		parent = GameObject.Find ("PLANET HOLDER");
		Cam = GameObject.FindWithTag ("StarCamera");

		spawnedList = new List<GameObject>();

		for(int i = 0; i < howManyStars; i++) 
		{
			//Spawn in random position
			//float randx = Random.Range(leftBound,rightBound);
			//float randy = Random.Range(lowerBound, upperBound);
			//float randz = Random.Range (farZBound, closeZBound);

			float screenX = Random.Range(-outsideCameraRange.x, Cam.camera.pixelWidth + outsideCameraRange.x);
			float screenY = Random.Range(-outsideCameraRange.y, Cam.camera.pixelHeight + outsideCameraRange.y);
			float ScreenZ = Random.Range(10, farSpawnPlane);
			//	float ScreenZ = Random.Range(10, 100);//Random.Range(Cam.camera.nearClipPlane, Cam.camera.farClipPlane);

			Vector3 point = Cam.camera.ScreenToWorldPoint(new Vector3(screenX, screenY, ScreenZ));



			GameObject g = (GameObject)Instantiate(star, point, Quaternion.identity);
			g.transform.parent = parent.transform;

			g.GetComponent<SpaceStar>().outsideCamRange = outsideCameraRange;
			spawnedList.Add (g);
		}
	}

	void OnLevelWasLoaded(){
		if (Application.loadedLevelName == "buttons"){
			Spawn ();
		}
	}

}
