﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour {

	public GameObject Body;

	public bool toggleButton = true;

	public float DepressDistance;
	public float PressDownEnd;
	public float PressDownSpeed;
	public float RaiseTween;

	public float OnDistance;

	public bool DontChange = false;
	public bool ChangeTexture = false;
	public Color OffColor;
	public Color OnColor;
	public Material OffTex;
	public Material OnTex;

	public Color glowTintColor;
	Color tintOff;
	Color tintOn;
	public bool glow = false;

	public AudioClip OnSound;
	public AudioClip OffSound;

	public GameObject PANEL;
	public float sendVal;

	public Panel panelScript;

	[System.NonSerialized]
	public bool ACTIVE = false;
	 
	Vector3 StartLocalPos;
	void Start () {

		if (PANEL)
			panelScript = PANEL.GetComponent<Panel>();
		else
			panelScript = GameObject.Find ("PowerPanel").GetComponent<Panel>();

		

		StartLocalPos = transform.localPosition;

		if (ACTIVE){
			TurnOn ();
		}
		else{
			TurnOff ();
		}
		
		prev = !glow;
	}

	public bool GetState(){
		return ACTIVE;
	}

	void SetToggle(){
		toggleButton = true;
	}
	void SetNotToggle(){
		TurnOff ();

		toggleButton = false;
	}

	public void TurnOn(){

		//not super good, all sounds should probably be played through panels so buttons dont need to do this
		if(panelScript.currentPower > 0){

			if(this.name == "Wiper_Button")	{
				if(panelScript.currentPower > 0)
					GameObject.Find ("Wiper").GetComponent<Wiper>().Wipe ();
			}
			if (OnSound){
				audio.PlayOneShot (OnSound);
			}

		}




		if (PANEL){
			PANEL.SendMessage ("ClickOn",sendVal,SendMessageOptions.DontRequireReceiver);
		}

		ACTIVE = true;

		if (!DontChange){

			if (!ChangeTexture) {
				Body.renderer.material.color = tintOn;
			}
			else{
				//Body.renderer.material.mainTexture = OnTex;
				Body.renderer.material = OnTex;
				Body.renderer.material.color = tintOn;
			}
		}
	}

	public void TurnOff(){
		if (!ACTIVE && Time.time > 1f)
			return;

		if(panelScript.currentPower > 0){

			if (OffSound){
				if(Time.time > 1)
					audio.PlayOneShot (OffSound);
			}
		}
		ACTIVE = false;

		if (PANEL){
			PANEL.SendMessage ("ClickOff",sendVal,SendMessageOptions.DontRequireReceiver);
		}

		if (!DontChange){

			if (!ChangeTexture) {
				Body.renderer.material.color = tintOff;
			}
			else{
				//Body.renderer.material.mainTexture = OffTex;
				Body.renderer.material = OffTex;
				Body.renderer.material.color = tintOff;
			}
		}
	}

	bool prev = false;
	void DoGlow(){
		if (prev != glow){
			prev = glow;

			if (glow){
				tintOn = (OnColor + glowTintColor) / 2;
				tintOff = (OffColor + glowTintColor) / 2;
			}
			else{
				tintOn = OnColor;
				tintOff = OffColor;
			}
			if (!DontChange){

				if (ACTIVE){
					Body.renderer.material.color = tintOn;
				}
				else{
					Body.renderer.material.color = tintOff;
				}
			}
		}
	}

	bool held = false;
	void Touched(){
		held = true;
	}

	void Release(){
		held = false;
		
		Vector3 g = transform.localPosition;
		g.z = StartLocalPos.z + DepressDistance;
		transform.localPosition = g;
		
		if(toggleButton && panelScript.currentPower > 0){
			if(ACTIVE){
				TurnOff ();
			}
			else{
				TurnOn();
			}
		}
		else{
			if (ACTIVE){
				TurnOff();
			}
		}
	}

	float DistanceFromNeutral(Vector3 v){
		return Mathf.Abs(v.z - StartLocalPos.z );
	}

	void TweenLocalTowards(Vector3 v){
		Vector3 d = v - transform.localPosition;
		Vector3 c = d * RaiseTween;
		transform.localPosition += c * Time.deltaTime * 60;
	}

	void Update () {

		if (held){
			Vector3 c = transform.localPosition;
			if (DistanceFromNeutral(c) > PressDownEnd){
				c.z = StartLocalPos.z + DepressDistance;
			}
			else if (DistanceFromNeutral(c) < PressDownEnd){
				c.z = c.z + (PressDownSpeed * Time.deltaTime * 60);
			}
			transform.localPosition = c;

			if(!toggleButton){
				if (!ACTIVE){
					TurnOn();
				}
			}


			if (Input.GetButtonUp("Fire1")){
				Release ();
			}
		}
		else{
			if (ACTIVE){
				Vector3 t = StartLocalPos;
				t.z += OnDistance;
				TweenLocalTowards(t);
			}
			else{
				Vector3 t = StartLocalPos;
				TweenLocalTowards(t);
			}
		}

		DoGlow ();
	}
}
