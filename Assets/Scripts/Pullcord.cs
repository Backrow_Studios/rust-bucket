﻿using UnityEngine;
using System.Collections;

public class Pullcord : MonoBehaviour {
	
	public GameObject Cord;

	public bool stayPulled = false;

	public float slideRange;
	public float clunkYPos;
	public float clunkTween;
	public float startValue;

	public float returnTween;

	public float MoveUpTween;
	public float MaxUpStep;

	public float MoveTween;
	
	public float MaxStep;
	
	public GameObject PANEL;
	public int SendVal;

	public AudioClip[] soundList;

	float VALUE;
	
	bool held = false;
	
	Vector3 startpos;

	float MaxY;
	float MinY;
	
	GameObject cursorpoint;
	MouseFollow MF;
	void Start () {
		startpos = Cord.transform.localPosition;

		MaxY = startpos.y;
		MinY = startpos.y - slideRange;
		clunkYPos = startpos.y - clunkYPos;
	}

	
	GameObject cursor;
	void Touched(GameObject g){
		cursor = g;
		held = true;
		MF = cursor.GetComponent<MouseFollow>();
		cursorpoint = MF.GetComponent<MouseFollow>().MyPoint;

	}

	public float GetValue(){
		return VALUE;
	}

	bool reset = false;
	public void ResetPos(){
		reset = true;
	}

	void Update () {
		float prevVal = VALUE;
		if (held){
			Vector3 destination = MF.lineendpoint + (Cord.transform.position - cursor.transform.position);//Camera.main.ScreenToWorldPoint(target) - cursor.transform.localPosition;

			float d = destination.y - Cord.transform.position.y;
			float c = 0;

			if (d > 0){
				c = d * MoveUpTween;

				if (c < -MaxUpStep){
					c = -MaxUpStep;
				}
			}
			else{
				c = d * MoveTween;

				if (c > MaxStep){
					c = MaxStep;
				}
			}

			
			if ((Cord.transform.localPosition.y < clunkYPos) ){
				Vector3 target = Cord.transform.localPosition;
				target.y = MinY;
				Cord.transform.localPosition += ((target - Cord.transform.localPosition)*clunkTween);

			}
			else{
				Cord.transform.localPosition += new Vector3(0,c * Time.deltaTime * 60,0);
			}


			if (Cord.transform.localPosition.y > MaxY){
				Cord.transform.localPosition = new Vector3(Cord.transform.localPosition.x, MaxY,Cord.transform.localPosition.z);
			}
			if (Cord.transform.localPosition.y < MinY){
				Cord.transform.localPosition = new Vector3(Cord.transform.localPosition.x, MinY,Cord.transform.localPosition.z);
			}
			

			
			if (Input.GetButtonUp("Fire1")){
				held = false;
			}
		}
		else{
			if (!stayPulled || (Cord.transform.localPosition.y >= clunkYPos) || reset){
				float c = ((MaxY - Cord.transform.localPosition.y)* returnTween);
				Cord.transform.localPosition += new Vector3(0,c * Time.deltaTime * 60,0);
			}
			else{
				if ((Cord.transform.localPosition.y < clunkYPos) ){
					Vector3 target = Cord.transform.localPosition;
					target.y = MinY;
					Cord.transform.localPosition += ((target - Cord.transform.localPosition)*clunkTween);
					
				}
			}
		}

		//float range = MaxY - MinY;
		//float val = Cord.transform.localPosition.y - MinY;
		
		//VALUE = val/range;

		if ((Cord.transform.localPosition.y < clunkYPos) ){
			if (VALUE == 0){

				if (soundList.Length > 0){
					int rand = Random.Range (0,3);
					
					audio.PlayOneShot(soundList[rand]);
				}
			}
			VALUE = 1;
		}
		else{
			VALUE = 0;
		}

		if (PANEL){
			if (prevVal != VALUE){
				PANEL.SendMessage ("SliderInput", new Vector2(SendVal,VALUE),SendMessageOptions.DontRequireReceiver);
				reset = false;
			}
		}
		
	}
}
