﻿using UnityEngine;
using System.Collections;

public class TurnTableAnim : MonoBehaviour {


	Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	public void Open(){
		anim.SetBool("Open",true);
	}
	public void Close(){
		anim.SetBool("Open",false);
	}
}
