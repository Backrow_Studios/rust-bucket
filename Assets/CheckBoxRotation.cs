﻿using UnityEngine;
using System.Collections;

public class CheckBoxRotation : MonoBehaviour {

	public Vector2 startPositions;
	public float variance;

	void Start () {

		float var = Random.Range(-variance,variance);

		int r = Random.Range (0,2);

		if (r == 0)
			transform.localEulerAngles = new Vector3(0,0,startPositions.x + var);
		else{
			transform.localEulerAngles = new Vector3(0,0,startPositions.y + var);

		}
	}

}
