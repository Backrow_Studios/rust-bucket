using UnityEngine;
using System.Collections;

public class Knob : MonoBehaviour {

	public GameObject Body;

	public GameObject BreakablePart;
	public float breakthreshold;
	float breakcount;

	public float spinRange;

	public float startValue;

	public float spinTween;

	public bool radio;
	float test = 0;

	public float spinPow;
	public float rotMultiplier;

	public float MaxStep;
	
	public GameObject PANEL;
	public int SendVal;
	
	float VALUE;

	bool held = false;
	
	Quaternion startrot;
	
	float Max;
	float Min;

	float val = 0;


	GameObject rad;
	RadioPanel rad_script;
	BreakEquipment breaker;

	void Start () {

		rad = GameObject.Find ("Radio Panel");
		rad_script = rad.GetComponent<RadioPanel>();

		val = startValue;

		startrot = Body.transform.localRotation;
		Max = spinRange;
		Min = 0;

		float range = Max - Min;
		float n = Min + startValue * range;
		//transform.localRotation = Quaternion.Euler(0,0,n);

		if(BreakablePart){
			breaker = gameObject.GetComponent<BreakEquipment>();
		}


	}
	
	GameObject cursor;
	void Touched(GameObject g){
		cursor = g;
		held = true;
		MF = cursor.GetComponent<MouseFollow>();
		cursorpoint = MF.GetComponent<MouseFollow>().MyPoint;
	}
	
	public float GetValue(){
		return VALUE;
	}

	GameObject cursorpoint;
	MouseFollow MF;
	float c = 0;
	void Update () {
		float prevVal = VALUE;
		if (held){

			Vector3 destination = MF.lineendpoint + (transform.position - cursor.transform.position);//Camera.main.ScreenToWorldPoint(target) - cursor.transform.localPosition;

			Vector2 dir = new Vector2( MF.lineendpoint.x - cursorpoint.transform.position.x,
			                          MF.lineendpoint.y - cursorpoint.transform.position.y);

			float distance = Vector3.Distance(cursor.GetComponent<MouseFollow>().lineendpoint , cursorpoint.transform.position);

			float d;

			float x;
			float y;

			dir = dir * spinPow;



			if (Mathf.Abs (dir.x) > Mathf.Abs (dir.y)){
				d = -dir.x;
			}
			else{
				d = -dir.y;
			}

			c += d * Time.deltaTime * 60;// * spinTween;

			
			if (c > MaxStep){
				c = MaxStep;

			}
			if (c < -MaxStep){
				c = -MaxStep;

			}


			
			//transform.localPosition += new Vector3(c * Time.deltaTime * 60,0,0);

//
//			if (transform.localPosition.x > Max){
//				transform.localPosition = new Vector3(MaxX, transform.localPosition.y,transform.localPosition.z);
//			}
//			if (transform.localPosition.x < Min){
//				transform.localPosition = new Vector3(MinX, transform.localPosition.y,transform.localPosition.z);
//			}

	
			if (Input.GetButtonUp("Fire1")){
				held = false;
			}
		}

		c += ((0 - c)*spinTween) * Time.deltaTime * 60;

		transform.localRotation *= Quaternion.Euler (0,0,c * rotMultiplier * Time.deltaTime * 60);


		float range = Max - Min;
		val -= c;
		if(radio && Mathf.Abs (c) > 0.05)	{
			rad_script.frequency += (c * -0.01f);
		}

		if (BreakablePart){
			breakcount += Mathf.Abs (c);
			if (breakcount > breakthreshold){
				breaker.BreakOff(BreakablePart);
				held = false;
				this.enabled = false;
			}
		}

		if (val > Max){
			val = Max;
		}
		else if (val < Min){
			val = Min;
		}


		VALUE = val/range;

		if (PANEL){
			if (prevVal != VALUE){
				PANEL.SendMessage ("SliderInput", new Vector2(SendVal,VALUE),SendMessageOptions.DontRequireReceiver);
			}
		}
		
	}
}
