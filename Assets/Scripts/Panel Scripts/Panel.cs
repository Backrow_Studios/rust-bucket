﻿using UnityEngine;
using System.Collections;

public class Panel : MonoBehaviour {

	[System.NonSerialized]
	public PowerPanel myPowerPanel;
	[System.NonSerialized]
	public float currentPower;

	public void Start(){
		myPowerPanel = GameObject.Find ("PowerPanel").GetComponent<PowerPanel>();
	}

	public virtual void ClickOn(int v){
		if (v == 0){
			//dosomething
		}
	}
	public virtual void ClickOff(int v){
		if (v == 0){
			//dosomething
		}
	}

	public virtual void FlickOn(int v){
	
	}

	public virtual void FlickOff(int v){
	
	}


	public virtual void SliderInput(Vector2 v){
		if (v.x == 0){
			//do something with y
		}
	}

	public virtual void CrankInput(Vector2 v){

	}

	public void Update(){
		currentPower = myPowerPanel.PowerLevel;
	}
}
