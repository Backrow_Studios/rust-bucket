﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RadioPanel : Panel {


	public float max;
	public float min;
	public float StartValue = 90.0f;
	public float channel_volume;
	public float frequency;
	public float static_volume;
	public bool tuned_in;
	public bool power;
	public List<Planet_Radio> planets;
	public Planet_Radio p1;
	public Planet_Radio p2;
	public Planet_Radio p3;
	public Planet_Radio p4;
	public Planet_Radio p5;
	bool planets_added;

	void Start(){
		base.Start ();
		frequency = StartValue;
		channel_volume = 0.0f;
		static_volume = 0.0f;
		audio.volume = 0.0f;
		min = 86.0f;
		max = 92.0f;
		tuned_in = false;
		audio.mute = true;
		planets_added = false;
	}

	public float GetFrequency(){
		return frequency;
	}

	void Update () {
		static_volume = 1.0f - channel_volume;
		base.Update();
		//frequency = Mathf.Round (frequency * 100f) / 100f;
		if(power)	{
			audio.mute = false;
			Debug.Log (frequency);
			audio.volume = static_volume;
			if(tuned_in == false)	{
				audio.volume = 1.0f;
			}
	 	}
		else {
			audio.volume = 0.0f;
			audio.mute = true;
		}

		if (frequency > max){
			frequency = min;
		}
		if(frequency < min)	{
			frequency = max;
		}

	}
}
