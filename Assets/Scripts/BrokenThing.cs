﻿using UnityEngine;
using System.Collections;

public class BrokenThing : MonoBehaviour {
	public AudioClip GrabSound;
	public AudioClip HitSound;
	public Material myTexture = null;

	public float followtween = .05f;
	public float floatAboveAmount = -.15f;
	
	public float heldTween = .14f;
	public float rotateTween = .08f;
	
	[System.NonSerialized]
	public bool readyToDelete = false;
	
	AnalysisPanel analysisPanel;
	
	public string name = "SHIP \n EQUIPMENT";
	public string description = "You're not supposed to have this...";
	
	void Awake () {
		Physics.IgnoreLayerCollision(9,0);
		Physics.IgnoreLayerCollision(9,1);
		Physics.IgnoreLayerCollision(9,8,false);

		analysisPanel = GameObject.Find ("AnalysisPanel").GetComponent<AnalysisPanel>();
		
	}
	
	public float collisionSoundThreshold = .01f;
	
	void OnCollisionEnter(Collision col){
		if(!rigidbody.isKinematic){
			if (rigidbody.velocity.magnitude > collisionSoundThreshold){
				audio.PlayOneShot(HitSound);
			}	
			if (col.gameObject.name == "HoloEnd"){
				if (readyToDelete){
					analysisPanel.NewData(myTexture,name,description);
					Destroy (gameObject);
				}
			}
		}
		
	}

	void Release(Vector3 force){
		held = false;
		rigidbody.useGravity = true;
		rigidbody.freezeRotation = false;
		rigidbody.isKinematic = false;

		rigidbody.velocity = force;
	}

	void Update () {
		if (held){
			rigidbody.useGravity = false;
			rigidbody.isKinematic = true;
			rigidbody.freezeRotation = true;
			
			transform.position = cursor.transform.position; //+ dif;
			transform.position -= (Camera.main.transform.position-cursor.transform.position) * floatAboveAmount;
			
			Quaternion targetrot = cursor.transform.rotation;
			
			float angle = Quaternion.Angle(transform.rotation,targetrot);
			float step = angle * rotateTween;

			transform.rotation = Quaternion.RotateTowards(transform.rotation,targetrot,step * Time.deltaTime * 60);
		}
		if (Input.GetButtonUp("Fire1") && held){
			Release (Vector3.zero);
		}
		
	}
	
	Vector3 dif;
	bool held = false;
	GameObject cursor;
	void Touched(GameObject g){
		cursor = g;
		held = true;
		audio.PlayOneShot(GrabSound);
		
		//dif = transform.position - cursor.transform.position;
		//dif.z = 0;
	}
}
