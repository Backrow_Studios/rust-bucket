﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectiveManager : MonoBehaviour {

	public AudioClip completeSound;

	List<Objective> objectiveList = new List<Objective>();

	PowerPanel powerPanel;
	MovePanel movePanel;
	TakeOffPanel takeoffPanel;
	MouseFollow cursor;
	AnalysisPanel analysisPanel;
	PianoPanel pianoPanel;
	DJPanel djPanel;
	RadioPanel radioPanel;
	ToastPanel toastPanel;

	void Start() {
		powerPanel = GameObject.Find ("PowerPanel").GetComponent<PowerPanel>();
		movePanel = GameObject.Find ("MovePanel").GetComponent<MovePanel>();
		takeoffPanel = GameObject.Find ("TakeOffPanel").GetComponent<TakeOffPanel>();
		cursor = GameObject.Find ("Cursor").GetComponent<MouseFollow>();
		analysisPanel = GameObject.Find ("AnalysisPanel").GetComponent<AnalysisPanel>();
		pianoPanel = GameObject.Find ("PianoPanel").GetComponent<PianoPanel>();
		djPanel = GameObject.Find ("DJPanel").GetComponent<DJPanel>();
		radioPanel = GameObject.Find ("Radio Panel").GetComponent<RadioPanel>();
		toastPanel = GameObject.Find ("ToastPanel").GetComponent<ToastPanel>();

		foreach (Transform c in transform){
			if (c.gameObject.GetComponent<Objective>())
				objectiveList.Add(c.gameObject.GetComponent<Objective>());
		}

	}

	List<string> clearedList = new List<string>();
	List<string> visitedList = new List<string>();

	void ClearObjective(string name){
		if (!clearedList.Contains (name)){
			audio.PlayOneShot (completeSound);
			clearedList.Add (name);
			name = name.ToLower();
			foreach (Objective o in objectiveList){
				if(o.objName.ToLower() == name){
					o.Check();
				}
			}
		}
	}

	void OnLevelWasLoaded(){
		if (!visitedList.Contains (Application.loadedLevelName)){
			visitedList.Add (Application.loadedLevelName);
		}
	}

	int reqplanetAmt = 4;

	void Update () {
		if (powerPanel.currentPower > .1f){
			ClearObjective("power on");
		}
		if (movePanel.readyLandPlanet){
			ClearObjective("explorer");
		}
		if (takeoffPanel.onplanet){
			ClearObjective("touch down");
			if (takeoffPanel.takeoffscript.launch){
				ClearObjective("ready for takeoff");
			}
		}
		//CHECK NAME OF PLANET FOR STRAWBERRY FIELDS
		if (Application.loadedLevelName == "Strawberry_Planet"){
			ClearObjective("strawberry fields");
		}
		//Poll visited planets
		if (visitedList.Count >= reqplanetAmt){
			ClearObjective("sightseer");
		}
		if (cursor.GrabbedObject){
			if (cursor.GrabbedObject.name == "HoloCube"){
				ClearObjective("take a sample");
			}
			if (cursor.GrabbedObject.name == "brokenthing"){
				ClearObjective("oops");
			}
		}
		if (analysisPanel.used){
			ClearObjective("analyze it");
		}
		if (toastPanel.totalToastsCreated > 1){
			ClearObjective("toaster novice");
		}
		if (toastPanel.totalToastsCreated > 20){
			ClearObjective("toast master");
		}
		if (toastPanel.totalToastsCreated > 40){
			ClearObjective("toast god");
		}

		if (pianoPanel.slideout){
			ClearObjective("mozart");
		}

		if (djPanel.slideout){
			ClearObjective("mix it up");
		}
		if (radioPanel.tuned_in){
			ClearObjective("tune in");
		}
		if (cursor.totalButtonPresses > 100){
			ClearObjective("button masher");
		}
		if (movePanel.hitWarpSpeed){
			ClearObjective("warp speed");
		}
	}
}
