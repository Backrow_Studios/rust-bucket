﻿using UnityEngine;
using System.Collections;

public class Star : MonoBehaviour {

	float distance;

	//var for x bounds ( -110 - 110)
	int leftBound;
	int rightBound;

	//var for y bounds
	int upperBound;
	int lowerBound;

	//var for z bounds
	int farZBound;
	int closeZBound;
	
	StarSpawner script;
	public Vector3 directionSpeed;

	// Use this for initialization
	void Start () {
		//Taking variables from starspawner scirpt
		GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");

		Transform theTransform = camera.GetComponent<Transform>();

		script = camera.GetComponent<StarSpawner>();
		distance = (transform.position.z - theTransform.position.z);

		leftBound = script.leftBound;
		rightBound = script.rightBound;
		upperBound = script.upperBound;
		lowerBound = script.lowerBound;
		farZBound = script.farZBound;
		closeZBound = script.closeZBound;
	}
	
	// Update is called once per frame
	void Update () {

		//MOVE

		//moved here instead of start to update in realtime changes
		directionSpeed = script.directionSpeed;

		//move star to the left by speed
		transform.position += (directionSpeed * Time.deltaTime * 60);


		//IF OFF BOUNDS

		//if off left bounds teleport to right bounds
		if(transform.position.x < leftBound)
		{
			float height = transform.position.y;
			float depth = transform.position.z;
			transform.position = new Vector3 (rightBound, height, depth);
		}
		//if off right bounds teleport to left
		else if(transform.position.x > rightBound)
		{
			float height = transform.position.y;
			float depth = transform.position.z;
			transform.position = new Vector3 (leftBound, height, depth);

		}
		//if off upper bounds teleport to under
		else if(transform.position.y > upperBound)
		{
			float xCord = transform.position.x;
			float depth = transform.position.z;
			transform.position = new Vector3(xCord, lowerBound, depth);
		}

		//if off lower bounds teleport to upper
		else if(transform.position.y < lowerBound)
		{
			float xCord = transform.position.x;
			float depth = transform.position.z;
			transform.position = new Vector3(xCord, upperBound, depth);
		}

		//if off the close bound teleport to far bound
		else if(transform.position.z < closeZBound)
		{
			float xCord = transform.position.x;
			float height = transform.position.y;
			transform.position = new Vector3(xCord, height, farZBound);
		}

		//if off the far bound teleport to close bound
		else if(transform.position.z > farZBound)
		{
			float xCord = transform.position.x;
			float height = transform.position.y;
			transform.position = new Vector3(xCord, height, closeZBound);
		}
	}
}
