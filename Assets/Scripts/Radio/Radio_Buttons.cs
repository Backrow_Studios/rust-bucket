﻿using UnityEngine;
using System.Collections;

public class Radio_Buttons : MonoBehaviour {
	
	public GameObject Body;
	
	public bool toggleButton = true;
	
	public float DepressDistance;
	public float PressDownEnd;
	public float PressDownSpeed;
	public float RaiseTween;
	
	public float OnDistance;
	
	public bool ChangeTexture = false;
	public Color OffColor;
	public Color OnColor;
	public Texture OffTex;
	public Texture OnTex;
	
	public AudioClip OnSound;
	public AudioClip OffSound;
	
	public GameObject PANEL;
	public float sendVal;
	
	[System.NonSerialized]
	public bool ACTIVE = false;

	public Panel panelScript;
	
	Vector3 StartLocalPos;

	GameObject rad;
	RadioPanel rad_script;
	void Start () {
		if (PANEL)
			panelScript = PANEL.GetComponent<Panel>();
		else
			panelScript = GameObject.Find ("PowerPanel").GetComponent<Panel>();

		rad = GameObject.Find ("Radio Panel");
		rad_script = rad.GetComponent<RadioPanel>();

		StartLocalPos = transform.localPosition;
		
		if (ACTIVE){
			TurnOn ();
		}
		else{
			TurnOff ();
		}
	}
	
	public bool GetState(){
		return ACTIVE;
	}
	
	public void TurnOn(){
		if(name == "Radio_Power"){
			if(panelScript.currentPower > 0){
				if(rad_script.power == false)
					rad_script.power = true;
			}
		}

		ACTIVE = true;
		
		
		if (!ChangeTexture) {
			Body.renderer.material.color = OnColor;
		}
		else{
			Body.renderer.material.mainTexture = OnTex;
		}
	}
	
	public void TurnOff(){
		if (OffSound){
			audio.PlayOneShot (OffSound);
		}
		ACTIVE = false;

		if (name == "Radio_Power") {
			if (rad_script.power == true)
				rad_script.power = false;
		}

		if (PANEL){
			PANEL.SendMessage ("ClickOff",sendVal,SendMessageOptions.DontRequireReceiver);
		}
		
		if (!ChangeTexture) {
			Body.renderer.material.color = OffColor;
		}
		else{
			Body.renderer.material.mainTexture = OffTex;
		}
	}
	
	bool held = false;
	void Touched(){
		held = true;
	}
	
	float DistanceFromNeutral(Vector3 v){
		return Mathf.Abs(v.z - StartLocalPos.z );
	}
	
	void TweenLocalTowards(Vector3 v){
		Vector3 d = v - transform.localPosition;
		Vector3 c = d * RaiseTween;
		transform.localPosition += c * Time.deltaTime * 60;
	}
	
	void Update () {
		
		if (held){
			Vector3 c = transform.localPosition;
			if (DistanceFromNeutral(c) > PressDownEnd){
				c.z = StartLocalPos.z + DepressDistance;
			}
			else if (DistanceFromNeutral(c) < PressDownEnd){
				c.z = c.z + (PressDownSpeed * Time.deltaTime * 60);
			}
			transform.localPosition = c;
			
			if(!toggleButton){
				if (!ACTIVE){
					TurnOn();
				}
			}
			
			
			if (Input.GetButtonUp("Fire1")){
				held = false;
				
				Vector3 g = transform.localPosition;
				g.z = StartLocalPos.z + DepressDistance;
				transform.localPosition = g;
				
				if(toggleButton){
					if(ACTIVE){
						TurnOff ();
					}
					else{
						TurnOn();
					}
				}
				else{
					if (ACTIVE){
						TurnOff();
					}
				}
			}
		}
		else{
			if (ACTIVE){
				Vector3 t = StartLocalPos;
				t.z += OnDistance;
				TweenLocalTowards(t);
			}
			else{
				Vector3 t = StartLocalPos;
				TweenLocalTowards(t);
			}
		}
	}
}
