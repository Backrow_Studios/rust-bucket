﻿using UnityEngine;
using System.Collections;

public class Land : MonoBehaviour {

	public bool landed = false;

	DontDestroyMeOnLoad scrip;

	[System.NonSerialized]
	public AnimationCurve speedCurve;

	public float speed;
	public float landSpeed = 5f;

	public float sky_speed;

	Vector3 up = Vector3.up;
	
	public GameObject front;
	public GameObject mid;
	public GameObject back;
	public GameObject all;
	public GameObject cam;
	public GameObject sky;

	public float max_y;

	[System.NonSerialized]
	public bool failed = false;
	// Use this for initialization
	void Start () {
		speed = 5.0f;
		sky_speed = 8.74f;
		if(Application.loadedLevelName == "Desert Planet")
			max_y = 89.1337f;
		else
			max_y = 45.30799f;
		//max_y = 10.6f;
	}

	float distanceFromLand=0f;

	void OnLevelWasLoaded(){
		front = GameObject.Find ("Front_Row");
		mid = GameObject.Find ("Middle_Row");
		back = GameObject.Find ("Back_Row");
		all = GameObject.Find ("Scenary");
		cam = GameObject.Find("Main Camera");
		sky = GameObject.Find("Sky");
		scrip = GameObject.Find("SHIP").GetComponent<DontDestroyMeOnLoad>();
		if(scrip.beginning == true)	{
			landed = true;
			front.transform.localPosition = new Vector3(front.transform.localPosition.x, 10.70956f, front.transform.localPosition.z);
			mid.transform.localPosition = new Vector3(mid.transform.localPosition.x, 11.40355f, mid.transform.localPosition.z);
			back.transform.localPosition = new Vector3(back.transform.localPosition.x, 12.31975f, back.transform.localPosition.z);
		}

		distanceFromLand = Mathf.Abs(front.transform.localPosition.y - 2);
	}

	public float getAngle()	{
		return front.transform.rotation.z;
	}

	public void TurnRight(float angle,bool useCurve)	{

		if (useCurve){
			float eval = Mathf.Abs(front.transform.localPosition.y - 2) / distanceFromLand;
			angle = ((speedCurve.Evaluate(1 - eval) * angle) + angle)/2;
		}

		if(landed == false)	{
			if(front.transform.localPosition.y < max_y && speed > 0)	{
				//if(Application.loadedLevelName == "Desert Planet")
				//	max_y = 89.1337f - front.transform.rotation.eulerAngles.z / 30f;
				//else
				//	max_y = 45.30799f - front.transform.rotation.eulerAngles.z / 30f;
				front.transform.Rotate (new Vector3(0, 0, angle));
				mid.transform.Rotate (new Vector3(0, 0, angle));
				back.transform.Rotate (new Vector3(0, 0, angle));
			}
		}
	}
	
	public void TurnLeft(float angle,bool useCurve)	{
		if (useCurve){
			float eval = Mathf.Abs(front.transform.localPosition.y - 2) / distanceFromLand;
			angle = ((speedCurve.Evaluate(1 - eval) * angle) + angle)/2;
		}

		if(landed == false)	{
			if(front.transform.localPosition.y < max_y && speed > 0)	{
				//if(Application.loadedLevelName == "Desert Planet")
				//	max_y = 89.1337f - front.transform.rotation.eulerAngles.z / 30f;
				//else
				//	max_y = 45.30799f - front.transform.rotation.eulerAngles.z / 30f;
				front.transform.Rotate (new Vector3(0, 0, -1f * angle));
				mid.transform.Rotate (new Vector3(0, 0, -1f * angle));
				back.transform.Rotate (new Vector3(0, 0, -1f * angle));
			}
		}
	}


	void Update () {
		float step = 5f * Time.deltaTime;
		if(landed == false)	{
			if(front.transform.localPosition.y < 2){
				//if (failed){ //I want the ship to fall faster when you fail the landing game, but i'm not sure if this does anything
				//	speed = 7.5f;
				//}
				//else{
				//	speed = 5.0f;
				//}

				float eval = Mathf.Abs(front.transform.localPosition.y - 2) / distanceFromLand;
				speed = speedCurve.Evaluate(1 - eval) * landSpeed;

				if (failed){
					speed = landSpeed * 2;
				}
			}
			else {
				speed -= 0.01f;
			}

			if(front.transform.localPosition.y < max_y && speed > 0)	{
				front.transform.position += (speed * up * Time.deltaTime);
				mid.transform.position += (speed * up * Time.deltaTime);
				back.transform.position += (speed * up * Time.deltaTime);
				if(Application.loadedLevelName != "Water_Planet")
					sky.transform.position += (speed * up * Time.deltaTime);
				else
					sky.transform.position += (sky_speed * Vector3.back * Time.deltaTime * 0.5f);
			}
			else
				landed = true;
		}
		else{
			if (!failed){
				if(front.transform.rotation.z > 0 || front.transform.rotation.z < 0)	{
					//Debug.Log (max_y);
					front.transform.rotation = Quaternion.RotateTowards (front.transform.rotation, Quaternion.Euler (Vector3.up), step);
					mid.transform.rotation = Quaternion.RotateTowards (mid.transform.rotation, Quaternion.Euler (Vector3.up), step);
					back.transform.rotation = Quaternion.RotateTowards (back.transform.rotation, Quaternion.Euler (Vector3.up), step);
				}
			}
		}
	}
}
