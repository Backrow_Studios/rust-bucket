using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerPanel : Panel {

	public GameObject light;

	public GameObject blinker;
	Light blinkLight;

	Light lightcomponent;

	public List<GameObject> meterObjects;
	List<Meter> meters;

	public float chargeSpeed;
	float powerlevel = 0;
	float meterAmount;

	public float powerDecay;

	public float lightFlickerOnSpeed = .1f;
	public float lightFlickerFreq = .1f;


	public Vector2[] decayRates;

	float onLightIntensity;
	void Start(){
		base.Start();
		lightcomponent  = light.GetComponent<Light>();
		onLightIntensity = lightcomponent.intensity;
		lightcomponent.intensity = onLightIntensity  * .1f;

		meters = new List<Meter>();

		foreach (GameObject g in meterObjects){
			meters.Add (g.GetComponent<Meter>());

		}
		meterAmount = meters.Count;

		blinkLight = blinker.GetComponent<Light>();

	}
	bool on = false;
	public override void CrankInput(Vector2 v){
		v.y = Mathf.Abs (v.y);
		if (chargable){
			powerlevel += v.y  * chargeSpeed;
			if (powerlevel > 1){
				powerlevel = 1;
			}

			if (!on){
				if (powerlevel > .12f){
					TurnOn();
				}
			}
			else{
				if (powerlevel <= .12f){
					TurnOff();
				}
			}
		}
	}

	bool flickerlights = false;

	void TurnOn(){
		on = true;
		//lightcomponent.intensity = onLightIntensity;
		flickerlights = true;
		//audio.Play ();
	}
	void TurnOff(){
		//audio.Stop ();
		on = false;
		lightcomponent.intensity = onLightIntensity  * .1f;
	}


	public float PowerLevel{
		get{
			return powerlevel;
		}
	}

	bool chargable = true;

	public override void ClickOn(int v){
		if (v == 0){
			//chargable = true;
		}
		
	}
	public override void ClickOff(int v){
		if (v == 0){
			//chargable = false;

		}
	}	

	float flickermult = 0f;
	bool f = false;
	float flickercount = 0f;

	void LightFlicker(){
		if (flickerlights){

			flickercount += Time.deltaTime;

			if (flickercount > (lightFlickerFreq / flickermult)){
				flickercount = 0;
			
				if (!f){
					lightcomponent.intensity = onLightIntensity  * .1f;;
					f = true;
				}
				else{
					flickermult += lightFlickerOnSpeed * Time.deltaTime * 60;
					lightcomponent.intensity = onLightIntensity  * flickermult;

					f = false;
				}
				if (flickermult >= .8f){
					lightcomponent.intensity = onLightIntensity;
					flickerlights = false;
					blinker.renderer.material.color = blinkOff;
					blinkLight.enabled = false;
				}

			}
		}
		else{
			flickermult = .1f;
		}
	}

	float blinkcount = 0f;
	public float blinkFreq = 1f;
	public Color blinkOn;
	public Color blinkOff;
	bool blink = false;
	void DoBlinking(){
		blinkcount += Time.deltaTime;
		if (blinkcount > blinkFreq){
			blinkcount = 0;
			if (!blink){
				blinker.renderer.material.color = blinkOn;
				blink = true;
				blinkLight.enabled = true;

			}
			else{
				blinker.renderer.material.color = blinkOff;
				blink = false;
				blinkLight.enabled = false;

			}

		}

	}

	void Update(){
		base.Update();

		LightFlicker();

		float fillbank = powerlevel;


		if (powerlevel <= 0){
			powerlevel = 0;
			DoBlinking();
		}
		else{
			for (int p = (int)(decayRates.Length - 1); p >= 0; p--){
				if (powerlevel > decayRates[p].x){
					powerlevel -= decayRates[p].y;
					p = -1;
				}

			}

		}

		if (on){
			if (powerlevel <= .12f){
				TurnOff();
			}
		}

		for (int i = 0; i < meterAmount; i++){

			float fill = fillbank - (1/meterAmount);

			if (fill < 0){
				fill = 0;
			}

			float added = fillbank - fill;

			fillbank = fill;

			meters[i].FillLevel = added * meterAmount;
		
		}
		
	}
}
