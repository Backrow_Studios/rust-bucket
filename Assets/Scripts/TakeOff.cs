﻿using UnityEngine;
using System.Collections;

public class TakeOff : MonoBehaviour {

	public Flash flashScript;

	public bool rumbled;
	public bool launch = false;

	float front_speed;
	float mid_speed;
	float back_speed;
	float launch_speed;
	float sky_speed;

	Vector3 forward = Vector3.forward * -1f;
	Vector3 down = Vector3.down;

	Land land_script;

	public GameObject front;
	public GameObject mid;
	public GameObject back;
	public GameObject all;
	public GameObject cam;
	public GameObject sky;

	void Start () {
		rumbled = false;
		front_speed = 4.0f;
		mid_speed = 2.5f;
		back_speed = 2.0f;
		sky_speed = 3f;
		launch_speed = 10.0f;

		flashScript = GameObject.Find ("Flasher").GetComponent<Flash>();

	}

	void Update () {
		front = GameObject.Find ("Front_Row");
		mid = GameObject.Find ("Middle_Row");
		back = GameObject.Find ("Back_Row");
		all = GameObject.Find ("Scenary");
		cam = GameObject.Find("Main Camera");
		sky = GameObject.Find ("Sky");

		float step = 5f * Time.deltaTime;

		land_script = this.GetComponent<Land>();

		if(Input.GetKey (KeyCode.Space)){
			if(land_script.landed == true)
		   		launch = true;
		}

		if(launch == true)	{
//			if(front.transform.rotation.z > 0 || front.transform.rotation.z < 0)	{
//				Debug.Log (front.transform.rotation.z);
//				front.transform.rotation = Quaternion.RotateTowards (front.transform.rotation, Quaternion.Euler (Vector3.up), step);
//				mid.transform.rotation = Quaternion.RotateTowards (mid.transform.rotation, Quaternion.Euler (Vector3.up), step);
//				back.transform.rotation = Quaternion.RotateTowards (back.transform.rotation, Quaternion.Euler (Vector3.up), step);
//			}
			//move the rows down until the back row reaches the threshold
			if(back.transform.localPosition.y > 0f)	{
				front.transform.position += ( front_speed * down * Time.deltaTime);
				mid.transform.position += ( mid_speed * down * Time.deltaTime);
				back.transform.position += ( back_speed * down * Time.deltaTime);
				if(Application.loadedLevelName != "Water_Planet")
					sky.transform.position += (sky_speed * down * Time.deltaTime);
				else
					sky.transform.position += (sky_speed * Vector3.forward * Time.deltaTime);
			}
			//once the rows are done moving down, start moving towards space
			else {
				//adjust speed based on position
				if(all.transform.localPosition.z < 10.0f)	{
					launch_speed = 16.0f;
				}
				else
					launch_speed = 10.0f;

				//move towards space, once threshold has been reached, go to "space level"
				if(all.transform.localPosition.z > -50.0f)	{
					all.transform.position += ( launch_speed * forward * Time.deltaTime);
				}
				else{
					flashScript.DoFlash();
					Application.LoadLevel ("buttons");
				}
			}


		}
	
	
	}


}