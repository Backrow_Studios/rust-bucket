﻿using UnityEngine;
using System.Collections;

public class ApertureOpen : MonoBehaviour {

	public AudioClip openSound;
	public AudioClip closeSound;

	public GameObject Aperture;
	public Animator animator;
	void Start(){
		animator = Aperture.GetComponent<Animator>();
		animator.speed = 2f;
	}

	void OnTriggerEnter(Collider col){
		if (col.tag == "physical"){

			if (!animator.GetBool("Open")){
				animator.SetBool("Open",true);
				audio.PlayOneShot (openSound);
			}
		}
	}
	void OnTriggerExit(Collider col){
		if (col.tag == "physical"){
			if (animator.GetBool("Open")){
				animator.SetBool("Open",false);
				audio.PlayOneShot (closeSound);
			}
		}
	}
}
