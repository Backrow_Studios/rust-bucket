﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovePanel : Panel {

	public Flash flashScript;

	public AudioClip drone_sound;
	public AudioClip strain_sound;
	public AudioClip error_sound;
	AudioSource ship_sound;
	

	public GameObject navPanel;
	public float moveSpeed = 10;
	public float decelTween = .1f;
	bool GO = false;

	float[] startlist = new float[2];

	public GameObject flashButton;
	ButtonScript flashButtonScript;
	public float buttonFlashFreq;
	float flashcount = 0;

	public float reqBoostAmount = .8f;

	Camera camera;

	MoveCamera movecamera;
	SpaceStarSpawner spacestarspawner;
	NavPanel navpanel;

	public List<GameObject> graphs;
	List<LineGraph> graphList;
	public float graphTween;


	void Start(){
		base.Start ();
		camera = Camera.main;
		spacestarspawner = Camera.main.GetComponent<SpaceStarSpawner>();
		movecamera = Camera.main.GetComponent<MoveCamera>();
		navpanel = navPanel.GetComponent<NavPanel>();

		ship_sound = GameObject.Find ("SHIP").GetComponent<AudioSource>();
		ship_sound.clip = drone_sound;
		ship_sound.panLevel = 0;
		ship_sound.volume = 0.5f; 
		ship_sound.loop = true;
		ship_sound.Stop ();

		for (int i = 0; i < startlist.Length; i++){
			startlist[i] = 0;
		}

		flashScript = GameObject.Find ("Flasher").GetComponent<Flash>();
		flashButtonScript = flashButton.GetComponent<ButtonScript>();

		empty = true;

		graphList = new List<LineGraph>();
		foreach (GameObject g in graphs){
			graphList.Add (g.GetComponent<LineGraph>());
		}
	}

	GameObject planetHolder;

	Vector3 savedPos;
	Quaternion savedRot;
	bool empty = false;

	void OnLevelWasLoaded(){
		if (Application.loadedLevelName == "buttons"){
			planetHolder = GameObject.Find ("PLANET HOLDER");

			if (empty){
				savedPos = planetHolder.transform.position;
				savedRot = planetHolder.transform.rotation;
				empty = false;
			}

			planetHolder.transform.position = savedPos;
			planetHolder.transform.rotation = savedRot;

		}
	}

	

	bool resetLevers = false;
	public override void SliderInput(Vector2 v){
		//for the slide levers
		if (navpanel.Targeted){
			startlist[(int)v.x] = (v.y + 1f) / 2;
		}
		else{
			resetLevers = true;
		}
	}

	[System.NonSerialized]
	public int landcount = 0;

	void ClickOn(int v){
		if (v == 0){
			if (boostmult > reqBoostAmount){
				if (navpanel.Targeted){
					ship_sound.Play ();
					GO = true;
					movecamera.ShakeAmount = 0;
					flash = false;
				}
			}
			else{
				if(error_sound)
					audio.PlayOneShot (error_sound);
			}

			foreach(Transform child in transform){
				child.SendMessage("ResetPos",SendMessageOptions.DontRequireReceiver);
			}

		}

//		if (v == 1){
//			landcount += 1;
//		}
//		if (v == 2){
//			landcount += 1;
//		}
//		if (v == 3){
//			landcount += 1;
//		}
	}
	void ClickOff(int v){
//		if (v == 1){
//			landcount -= 1;
//		}
//		if (v == 2){
//			landcount -= 1;
//		}
//		if (v == 3){
//			landcount -= 1;
//		}
//		if (landcount < 0)
//			landcount = 0;
	}

	public bool Moving{
		get{
			return GO;
		}
	}

	void StopMove(){
		GO = false;
		ship_sound.Stop();
		boostmult = 0;
		movecamera.ShakeAmount = 0;

	}



	void LandOn(GameObject p){
		string levelname = p.GetComponent<PlanetaryBody>().sceneToLoad;
		flashScript.DoFlash();

		if (planetHolder){
			//landstuff
		}

		Application.LoadLevel(levelname);
	}

	float boostmult = 0;

	[System.NonSerialized]
	public GameObject readyLandPlanet;

	bool rl = false;
	void DoLanding(){
		if (readyLandPlanet){
			if (!rl){
				rl = true;
				foreach(Transform child in transform){
					child.SendMessage("SetToggle",SendMessageOptions.DontRequireReceiver);
				}
				boostmult = 0;
				movecamera.ShakeAmount = 0;
			}

			if (landcount >= 4){
				LandOn (readyLandPlanet);
			}
			else{
				landcount = 0;
			}

			float distance = Vector3.Distance(readyLandPlanet.transform.position,camera.transform.position);
			if (distance > readyLandPlanet.GetComponent<PlanetaryBody>().landDistanceThreshold){
				readyLandPlanet = null;
			}

		}
		else{
			landcount = 0;
			if (rl){
				rl = false;
				foreach(Transform child in transform){
					child.SendMessage("SetNotToggle",SendMessageOptions.DontRequireReceiver);
				}
			}
		}

	}

	bool flash = false;
	void DoButtonFlash(){
		if (flash){
			flashcount += Time.deltaTime;

			if (flashcount > buttonFlashFreq){
				flashcount = 0;
				flashButtonScript.glow = !flashButtonScript.glow;
			}
		}
		else{
			flashButtonScript.glow = false;
		}
	}

	void KeepLeversDown(){
		if (!Input.GetButton("Fire1")){
			if (resetLevers){
				resetLevers = false;
				if(error_sound)
					audio.PlayOneShot (error_sound);
				
				foreach(Transform child in transform){
					child.SendMessage("ResetPos",SendMessageOptions.DontRequireReceiver);
				}
			}
		}
	}
	[System.NonSerialized]
	public bool hitWarpSpeed = false;

	float prevPower = 0;
	void Update () {
		base.Update ();
		if (currentPower > .08f){

			DoButtonFlash();
			KeepLeversDown();

			if (prevPower < .08f){
				//reset my switches if i was just turned on
				foreach(Transform child in transform){
					child.SendMessage("ResetPos",SendMessageOptions.DontRequireReceiver);
				}
			}
			if (!GO){

				float s = 0;
				
				foreach (float g in startlist){
					s += g;
				} 

				boostmult = s / startlist.Length;


				if ((boostmult > reqBoostAmount)){
					flash = true;
				}
				else{
					flash = false;
				}

				movecamera.ShakeAmount = boostmult * .05f;
				spacestarspawner.directionSpeed.z += ((0 - spacestarspawner.directionSpeed.z)* decelTween);
			}
			if (navpanel.SelectedPlanet){
				float distance = Vector3.Distance(navpanel.SelectedPlanet.transform.position,camera.transform.position);
				if (GO){

					float tempTween = decelTween;
					float mult = 1;
					if (currentPower > .85f){
						hitWarpSpeed = true;
						mult = 4;
					}
					else if (currentPower > .7f){
						mult = 2.5f;
					}
					else if (currentPower > .5f){
						mult = 2f;
					}
					else if (currentPower > .25f){
						mult = 1.5f;
					}
					else{
						mult = 1f;
					}
				
					if (distance < navpanel.SelectedPlanet.GetComponent<PlanetaryBody>().slowdownDistanceThreshold){
						boostmult = .15f;
						movecamera.ShakeAmount = boostmult * .05f;
						mult = 1;
						tempTween = decelTween * 2;
					}


					float target = -1 * moveSpeed * (boostmult * mult);


					spacestarspawner.directionSpeed.z += ((target - spacestarspawner.directionSpeed.z)* tempTween);

				}
				if (distance < navpanel.SelectedPlanet.GetComponent<PlanetaryBody>().landDistanceThreshold){
					//LandOn (navpanel.SelectedPlanet);
					StopMove();
					readyLandPlanet = navpanel.SelectedPlanet;
				}
			}

			DoLanding();

			graphList[0].wavelength += ((2 - graphList[0].wavelength) * graphTween) * Time.deltaTime * 60;
			graphList[0].amplitude += ((boostmult - graphList[0].amplitude) * graphTween) * Time.deltaTime * 60;
			graphList[0].scrollspeed += ((.3f - graphList[0].scrollspeed) * graphTween) * Time.deltaTime * 60;

			graphList[1].wavelength += (((boostmult + .1f) * 2 - graphList[1].wavelength) * graphTween) * Time.deltaTime * 60;
			graphList[1].amplitude += ((boostmult - graphList[1].amplitude) * graphTween) * Time.deltaTime * 60;
			graphList[1].scrollspeed += ((.3f - graphList[1].scrollspeed) * graphTween) * Time.deltaTime * 60;

		}
		else{
			for (int i = 0; i < graphList.Count; i++){
				graphList[i].wavelength += ((1 - graphList[i].wavelength) * graphTween) * Time.deltaTime * 60;
				graphList[i].amplitude += ((0 - graphList[i].amplitude) * graphTween) * Time.deltaTime * 60;
				graphList[i].scrollspeed += ((0 - graphList[i].scrollspeed) * graphTween) * Time.deltaTime * 60;
			}

			if (prevPower >= .08f){
				//reset my switches if i was just turned off
				foreach(Transform child in transform){
					child.SendMessage("ResetPos",SendMessageOptions.DontRequireReceiver);
				}
			}
		}
		prevPower = currentPower;
	}
}
