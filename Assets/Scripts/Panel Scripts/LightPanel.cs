﻿using UnityEngine;
using System.Collections;

public class LightPanel : Panel {

	public GameObject light;

	float r = 0;
	float g = 0;
	float b = 0;

	Light lightcomponent;
	void Start(){
		base.Start ();
		lightcomponent  = light.GetComponent<Light>();
		lightcomponent.color = new Color(r,g,b);
	}

	public override void SliderInput(Vector2 v){
		if (v.x == 0){
			r = v.y;
		}
		if (v.x == 1){
			g = v.y;
		}
		if (v.x == 2){
			b = v.y;
		}
	}

	// Update is called once per frame
	void Update () {
		base.Update();
		if (currentPower > .08f){
			lightcomponent.color = new Color(r,g,b);
		}
		else{
			lightcomponent.color = new Color(0,0,0);

		}
	}
}
