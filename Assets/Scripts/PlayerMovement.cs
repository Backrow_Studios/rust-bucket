﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	float speed = 2.0f;
	Vector3 dir;

	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.W))
		   dir = Vector3.forward;
		if(Input.GetKey(KeyCode.S))
			dir = Vector3.back;
		if(Input.GetKey(KeyCode.A))
			dir = Vector3.left;
		if(Input.GetKey(KeyCode.D))
			dir = Vector3.right;
		if(Input.GetKeyDown(KeyCode.Space))	{
			if(speed > 0)
				speed = 0;
			else
				speed = 2.0f;
		}

		transform.position += (dir * speed * Time.deltaTime);
	}
}
