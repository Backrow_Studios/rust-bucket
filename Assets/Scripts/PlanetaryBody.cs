﻿using UnityEngine;
using System.Collections;

public class PlanetaryBody : MonoBehaviour {

	public GameObject myBody;
	public Object starBody;
	public Texture2D mapGraphic;

	public float invisibleDistanceThreshold;
	public float starDistanceThreshold;
	public float landDistanceThreshold;
	public float slowdownDistanceThreshold = 500;

	public float planetStarDistance;

	public float graphicTween = .5f;

	public string sceneToLoad = "none";

	float distance = 0;
	GameObject starbody;

	void Start () {
		starbody = (GameObject)Instantiate (starBody);
		//starbody.transform.parent = transform;
		starbody.transform.localPosition = Vector3.zero;
		starbody.transform.localEulerAngles = Vector3.zero;

	}
	
	void Update () {
		distance = Vector3.Distance(transform.position,Camera.main.transform.position);
		Vector3 target = Camera.main.transform.position + (transform.position - Camera.main.transform.position).normalized * planetStarDistance;
		starbody.transform.position += ((target - starbody.transform.position) * graphicTween) * Time.deltaTime * 60;


		if (distance > starDistanceThreshold){
			myBody.renderer.enabled = false;
			starbody.renderer.enabled = true;
		}
		else{
			myBody.renderer.enabled = true;
			starbody.renderer.enabled = false;
		}

	}
}
