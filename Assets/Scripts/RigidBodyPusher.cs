﻿using UnityEngine;
using System.Collections;

public class RigidBodyPusher : MonoBehaviour {

	public float force;
	public bool useAddForce = false;
	void OnTriggerStay(Collider col){
		if (col.tag == "physical"){
			if (col.GetComponent<BrokenThing>()){
				if (!col.transform.rigidbody.isKinematic)
				if (useAddForce){
					col.transform.rigidbody.AddForce(transform.forward * force);
					col.transform.gameObject.GetComponent<BrokenThing>().readyToDelete = true;
				}
				else
					col.transform.rigidbody.velocity = transform.forward * force;
			}
			else{
				if (!col.transform.parent.rigidbody.isKinematic)
				if (useAddForce){
					col.transform.parent.rigidbody.AddForce(transform.forward * force);
					col.transform.parent.gameObject.GetComponent<HoloCube>().readyToDelete = true;
				}
				else
					col.transform.parent.rigidbody.velocity = transform.forward * force;
			}
		}
	}
}
