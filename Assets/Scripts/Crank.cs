﻿using UnityEngine;
using System.Collections;

public class Crank : MonoBehaviour {
	
	public GameObject Body;

	public GameObject crankCenterPoint;

	public float spinRange;
	

	public float spinTween;
	
	public bool radio;
	float test = 0;
	
	public float spinPow;
	public float rotMultiplier;
	
	public float MaxStep;
	
	public GameObject PANEL;
	public int SendVal;
	
	float VALUE;
	
	bool held = false;

	public float maxLeverageDistance;

	public AudioClip clickSound;
	public float clickFreq = 1;
	float clickcount = 0;
	
	GameObject cursor;

	GameObject rad;
	RadioPanel rad_script;
	
	void Start () {
		
		rad = GameObject.Find ("Radio Panel");
		rad_script = rad.GetComponent<RadioPanel>();
	}

	void Touched(GameObject g){
		cursor = g;
		held = true;
		MF = cursor.GetComponent<MouseFollow>();
		cursorpoint = MF.GetComponent<MouseFollow>().MyPoint;
	}
	
	public float GetValue(){
		return VALUE;
	}

	float SignedAngleBetween(Vector3 a, Vector3 b, Vector3 n){
		// angle in [0,180]
		float angle = Vector3.Angle(a,b);
		float sign = Mathf.Sign(Vector3.Dot(n,Vector3.Cross(a,b)));
		
		// angle in [-179,180]
		float signed_angle = angle * sign;
		
		// angle in [0,360] (not used but included here for completeness)
		//float angle360 =  (signed_angle + 180) % 360;
		
		return signed_angle;
	}
	
	GameObject cursorpoint;
	MouseFollow MF;
	float c = 0;

	bool lastsend = false;

	void Update () {
		float prevVal = VALUE;
		if (held){
	
			Vector3 grab = cursorpoint.transform.position - crankCenterPoint.transform.position;
			Vector3 mouse = MF.lineendpoint - crankCenterPoint.transform.position;

			float dif = SignedAngleBetween(grab,mouse,transform.forward);

			float distancefromcenter = Vector3.Distance(cursorpoint.transform.position, crankCenterPoint.transform.position);
			
			float leveragemult = distancefromcenter / maxLeverageDistance;

			float dir = dif * spinPow * leveragemult;



			c += dir * Time.deltaTime * 60;
			
			
			if (c > MaxStep){
				c = MaxStep;
				
			}
			if (c < -MaxStep){
				c = -MaxStep;
				
			}
			
			if (Input.GetButtonUp("Fire1")){
				held = false;
			}
		}
		
		c += ((0 - c)*spinTween) * Time.deltaTime * 60;
		
		transform.localRotation *= Quaternion.Euler (0,0,c * rotMultiplier * Time.deltaTime * 60);

		clickcount += Mathf.Abs(c);
		if (clickcount > clickFreq) {
			clickcount = 0;
			audio.PlayOneShot(clickSound);
		}

		if(radio && Mathf.Abs (c) > 0.05)	{
			rad_script.frequency += (c * -0.01f);
		}

		if (PANEL){
			if (Mathf.Abs(c) > .001f){
				PANEL.SendMessage ("CrankInput", new Vector2(SendVal, c ),SendMessageOptions.DontRequireReceiver);
				lastsend = true;
			}
			else if (lastsend){
				lastsend = false;
				PANEL.SendMessage ("CrankInput", new Vector2(SendVal, 0 ),SendMessageOptions.DontRequireReceiver);
			}
		}
		
	}
}
