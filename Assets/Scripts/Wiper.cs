﻿using UnityEngine;
using System.Collections;

public class Wiper : MonoBehaviour {
	Animator anim;

	public int wipe_hash = Animator.StringToHash ("More Wipe");
	public int wipe2_hash = Animator.StringToHash("Wiping");


	void Start () {
		anim = GameObject.Find ("Wiper").GetComponent<Animator>();
	}

	public void Wipe()	{
		anim = GameObject.Find ("Wiper").GetComponent<Animator>();
		Invoke ("TurnOff", 0.1f);
		anim.SetBool("Wiping", true);
	}

	public void TurnOff()	{
		anim = GameObject.Find ("Wiper").GetComponent<Animator>();
		anim.SetBool("Wiping", false);

	}

	// Update is called once per frame
	void Update () {

	}
}
